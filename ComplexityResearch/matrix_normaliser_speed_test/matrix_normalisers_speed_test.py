# -*- coding: utf-8 -*-
"""
Created on Fri Dec 26 20:34:00 2014

@author: Matthias
"""
import time
from random import *

n = 99999
numExtractions = 999999

unitList = range(n)

matrix = []
for i in range(n):
    matrix.append(unitList)


    
startTime = time.time()

for i in range(numExtractions):
    x = matrix[randint(1,n-1)][randint(1,n-1)]
    
stopTime = time.time()
differenceTime = stopTime - startTime
print 'matrix'
print differenceTime


startTime = time.time()

for i in range(numExtractions):
    x = unitList[randint(1,n-1)] 
    
stopTime = time.time()
differenceTime = stopTime - startTime
print 'normallist' 
print differenceTime
