import urllib2
import csv
import sys
import os
import os.path
from A_definitions import loadstocklist
from time import gmtime, strftime
import numpy as np

stocklist=loadstocklist(0)

#http://ichart.finance.yahoo.com/table.csv?s=YHOO

#stocklist=['AGB.BR','AVH.BR','BEFB.BR','BEK.BR','BLG.BR','COFB.BR','COL.BR','DEL.BR','DIE.BR','DL.BR','ELI.BR','GBL.BR','GSZ.BR','INT.BR','KBC.BR','SOL.BR','THR.BR','TNO.BR','UCB.BR','UMC.BR']


for i in range(len(stocklist)):
    label = str(stocklist[i])
    filename=label+'.txt'
    f=os.path.isfile(filename)

    date=strftime("%Y-%m-%d %H:%M:%S", gmtime())[:10]

    stockdate='0'
    
    if f:
        stockdate=np.loadtxt(filename,dtype='string', delimiter=',', skiprows=1, usecols=(0,), unpack=False)
        stockdate=stockdate[0]

        if not date==stockdate:
            os.remove(filename)
        

    f=os.path.isfile(filename)
    
    if f==False:
        
        url = str('http://ichart.finance.yahoo.com/table.csv?s='+ label)
        try:
            u = urllib2.urlopen(url)
            localFile = open('table.csv', 'w')
            localFile.write(u.read())
            localFile.close()
            
            os.rename('table.csv',filename)

        except urllib2.HTTPError:
            print filename, 'not found'

    




