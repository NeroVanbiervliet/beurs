from A_definitions import *
import numpy as np
import matplotlib.pyplot as plt

bel20 = loadstocklist(1)
for j in range(len(bel20)):

    stock = bel20[j]
    stockpath = 'data/stockfiles/' + stock + '.txt'
    check = os.path.isfile(stockpath)
    
    if check:
        stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        normalised_normal = stockfile_normaliser(stockfile1)
        normalised_nero = stockfile_normaliser_nero(stockfile1)
        
            
        plt.plot(normalised_normal)    
        plt.plot(normalised_nero)
            
        savePath = 'results/stock'+str(j)+'_'+stock.replace(".","_")
        print stock
        plt.savefig(savePath)
        plt.clf()
    else:
        warningMessage = 'Stock '+stock+' of bel20 not found in ' + stockpath
        print warningMessage

print 'Simulation Completed'