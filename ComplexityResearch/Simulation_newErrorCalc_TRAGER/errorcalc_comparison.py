from A_definitions import *
import numpy as np
import os.path
import random
import time

bel20 = loadstocklist(1)
stock = random.choice(bel20)
stockpath = 'data/stockfiles/' + stock + '.txt'
stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
stockfile = stockfile_normaliser(stockfile1)

theoryList = theoryGenerator()
end = 500

numIterations = 10000

startTimeNormal = time.time()
for i in range(numIterations):
    normalError = errorCalculator(stockfile,theoryList[0],end)
    endTimeNormal = time.time()
elapsedNormal = endTimeNormal - startTimeNormal
print 'total time normal', elapsedNormal
print 'error normal', normalError

# trager
startTimeNew = time.time()
for i in range(numIterations):
    newError = errorCalculatorNew(stockfile,theoryList[0],end)
    endTimeNew = time.time()
elapsedTimeNew = endTimeNew - startTimeNew
print 'total time new', elapsedTimeNew
print 'error new', newError


startTimeNew2 = time.time()
for i in range(numIterations):
    newError2 = errorCalculatorNew2(stockfile,theoryList[0],end)
    endTimeNew2 = time.time()
elapsedTimeNew2 = endTimeNew2 - startTimeNew2
print 'total time new2', elapsedTimeNew2
print 'error new2', newError2

startTimeNew3 = time.time()
for i in range(numIterations):
    newError3 = errorCalculatorNew2(stockfile,theoryList[0],end)
    endTimeNew3 = time.time()
elapsedTimeNew3 = endTimeNew3 - startTimeNew3
print 'total time new3', elapsedTimeNew3
print 'error new3', newError3