from A_definitions import *
import numpy as np
import urllib2
import os
import os.path
import random
from random import randint
import time

Pack1 = 10000 #methode 1
Pack2 = 10000 #methode 2
Pack3 = 10000 #methode 3
Pack4 = 10000 #methode 4
Pack5 = 10000 #methode 5
Pack6 = 10000

Pack1temp = 0 #methode 1
Pack2temp = 0 #methode 2
Pack3temp = 0 #methode 3
Pack4temp = 0 #methode 4
Pack5temp = 0 #methode 5
Pack6temp = 0

cost1 = 7.25
cost2 = 9.75

bel20 = loadstocklist(1)
AEX = loadstocklist(5)
FTSE = loadstocklist(4)
stocklist = [bel20,bel20,bel20] # geen vector, maar matrix

Pack1list = []
Pack2list = []
Pack3list = []
Pack4list = []
Pack5list = []
Pack6list = []


simname = 'Sim 5'
days = 0

# timing 
startTime = time.time()

while days < 40:
    days += 1
    print 'Days:', days

    Pack1 = Pack1 + Pack1temp
    Pack2 = Pack2 + Pack2temp
    Pack3 = Pack3 + Pack3temp
    Pack4 = Pack4 + Pack4temp
    Pack5 = Pack5 + Pack5temp
    Pack6 = Pack6 + Pack6temp

    Pack1temp = 0 
    Pack2temp = 0 
    Pack3temp = 0 
    Pack4temp = 0 
    Pack5temp = 0 
    Pack6temp = 0

    Pack1start = Pack1
    Pack2start = Pack2
    Pack3start = Pack3
    Pack4start = Pack4
    Pack5start = Pack5
    Pack6start = Pack6

    Pack1list.append(Pack1)
    Pack2list.append(Pack2)
    Pack3list.append(Pack3)
    Pack4list.append(Pack4)
    Pack5list.append(Pack5)
    Pack6list.append(Pack6)
    
    print 'Pack1:',Pack1
    print 'Pack2:',Pack2
    print 'Pack3:',Pack3
    print 'Pack4:',Pack4
    print 'Pack5:',Pack5
    print 'Pack6:',Pack6
    
    searching = True
    count = 0
    entry = randint(30,600)
    while searching and count < 20:
        count +=1
        m = randint(0,2) #random which market
        stock = random.choice(stocklist[m]) # kiest een random stock van de market m
        stockpath = 'data/stockfiles/' + stock + '.txt'
        check = os.path.isfile(stockpath)
        
        if check and max(Pack1,Pack2,Pack3,Pack4,Pack5,Pack6) > 1000 :
            
            stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
            if len(stockfile1) > 700:
                print stock
                stockfile = stockfile_normaliser(stockfile1)
                
                E,P,amount = method1special(stockfile[entry:],stock,entry)
                
                print 'amount:',amount
                
                if E and amount > 15:
                    EP=[]
                    for i in range(len(E)):
                        EP.append(E[i]*100*P[i]*100)
                    
                    print 'E:',max(E)
                    print 'P:',max(P)
                    print 'EP:',max(EP)

                    
                    for i in range(len(E)):
                        if abs(E[i] - max(E)) < 0.00001:
                            entry1 = i+1
                    print 'entry1:',entry1
                    minimum = 1000
                    for i in range(entry1):
                        if E[i] < minimum:
                            entry11 = i
                            minimum = E[i]
                    print 'entry11:',entry11
                    
                    for i in range(len(P)):
                        if abs(P[i] - max(P)) < 0.00001:
                            entry2 = i+1
                    print 'entry2:',entry2
                    minimum = 1000
                    for i in range(entry2):
                        if P[i] < minimum:
                            entry21 = i
                            minimum = P[i]
                    print 'entry21:',entry21

                    print 'winst/verlies na 20 dagen in %:', (stockfile[entry-20]-stockfile[entry])/stockfile[entry]*100
                    
                    
                    ## Pack1 strategy
                    if max(P) > 0.6 and max(E)>0.01 and Pack1 > 1000:
                        stop=False
                        for i in range(entry2):
                            gain= (stockfile[entry-i]-stockfile[entry])/stockfile[entry]
                            if gain > -0.03 and (not stop):
                                gain1 = (stockfile[entry-(i+1)]-stockfile[entry])/stockfile[entry]
                            else:
                                stop=True

                        invest = min(Pack1start/5*(1+(max(P)-0.6)*2),Pack1-10)
                        if invest < 2500:
                            cost = cost1
                        else:
                            cost = cost2
                        Pack1 = Pack1 - invest - cost
                        Pack1temp = Pack1temp + invest*(1+gain1)
                        
                    ## Pack2 strategy
                    if max(P) > 0.65 and max(E)>0.015 and Pack2 > 1000:
                        stop=False
                        for i in range(entry2):
                            gain= (stockfile[entry-i]-stockfile[entry])/stockfile[entry]
                            if gain > -0.03 and (not stop):
                                gain2 = (stockfile[entry-(i+1)]-stockfile[entry])/stockfile[entry]
                            else:
                                stop=True

                        invest = min(Pack2start/5*(1+(max(P)-0.6)*2),Pack2-10)
                        if invest < 2500:
                            cost = cost1
                        else:
                            cost = cost2
                        Pack2 = Pack2 - invest - cost
                        Pack2temp = Pack2temp + invest*(1+gain2)

                    ## Pack3 strategy
                    if max(P) > 0.65 and max(E)>0.015 and Pack3 > 1000:
                        stop=False
                        for i in range(entry2):
                            gain= (stockfile[entry-i]-stockfile[entry])/stockfile[entry]
                            if gain > -0.03 and (not stop):
                                gain3 = (stockfile[entry-(i+1)]-stockfile[entry])/stockfile[entry]
                            else:
                                stop=True

                        invest = min(Pack3start/5*(1+(max(P)-0.6)*5),Pack3-10)
                        if invest < 2500:
                            cost = cost1
                        else:
                            cost = cost2
                        Pack3 = Pack3 - invest - cost
                        Pack3temp = Pack3temp + invest*(1+gain3)
                        
                    ## Pack4 strategy
                    if max(P) > 0.7 and max(E)>0.02 and Pack4 > 1000:
                        stop=False
                        for i in range(entry2):
                            gain= (stockfile[entry-i]-stockfile[entry])/stockfile[entry]
                            if gain > -0.03 and (not stop):
                                gain4 = (stockfile[entry-(i+1)]-stockfile[entry])/stockfile[entry]
                            else:
                                stop=True

                        invest = min(Pack4start/5*(1+(max(P)-0.6)*2),Pack4-10)
                        if invest < 2500:
                            cost = cost1
                        else:
                            cost = cost2
                        Pack4 = Pack4 - invest - cost
                        Pack4temp = Pack4temp + invest*(1+gain4)
                        
                    ## Pack5 strategy
                    if max(E)>0.02 and Pack5 > 1000:
                        stop=False
                        for i in range(entry2):
                            gain= (stockfile[entry-i]-stockfile[entry])/stockfile[entry]
                            if gain > -0.03 and (not stop):
                                gain5 = (stockfile[entry-(i+1)]-stockfile[entry])/stockfile[entry]
                            else:
                                stop=True

                        invest = min(Pack5start/5*(1+(max(P)-0.6)*3),Pack5-10)
                        if invest < 2500:
                            cost = cost1
                        else:
                            cost = cost2
                        Pack5 = Pack5 - invest - cost
                        Pack5temp = Pack5temp + invest*(1+gain5)
                        
                    ## Pack6 strategy
                    if max(P) > 0.6 and Pack6 > 1000:
                        gain6= (stockfile[entry2-entry]-stockfile[entry])/stockfile[entry]
                        invest = min(Pack6start/5,Pack6-10)
                        if invest < 2500:
                            cost = cost1
                        else:
                            cost = cost2
                        Pack6 = Pack6 - invest - cost
                        Pack6temp = Pack6temp + invest*(1+gain6)

graphsaver(Pack1,'',simname + ' Pack1')
graphsaver(Pack2,'',simname + ' Pack2')
graphsaver(Pack3,'',simname + ' Pack3')
graphsaver(Pack4,'',simname + ' Pack4')
graphsaver(Pack5,'',simname + ' Pack5')
graphsaver(Pack6,'',simname + ' Pack6')

# timing
endTime = time.time()
elapsedTime = endTime-startTime
print 'Total time spent on simulation in seconds:',elapsedTime
