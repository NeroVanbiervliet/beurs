from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import datetime
import pickle
import numpy as np

def least_squares_fit(stock):
    x = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35]
    errorBest = 999999
    
    for days1 in [15,25,35]:
        for days2 in [15,25,35]:
            for days3 in [15,25,35]:
    
                x1 = x[:days1]
                x2 = x[:days2+1]
                x3 = x[:days3+1]

                [c1,b1,a1],[R1],do,re,mi = np.polyfit(x1,stock[:days1],2,full=True)
                [c2,b2,a2],[R2],do,re,mi = np.polyfit(x2,stock[days1-1:days2+days1],2,full=True)
                [c3,b3,a3],[R3],do,re,mi = np.polyfit(x3,stock[days2+days1-1:days3+days1+days2],2,full=True)

                error = R1/(days1**1.3) + R2/(days2**1.3) + R3/(days3**1.3)
                
                if error < errorBest:
                    theory=[a1,b1,c1,days1,a2,b2,c2,days2,a3,b3,c3,days3]
                    errorBest=error
                    
    print theory[3]
    print theory[7]
    print theory[11]
    print ' '
    
    return theory
    
# # testje
    
# fakeStock = [1000,1020,1035,1040,1045,1060,940,940,920,940,940,960,965,955,970,990,1005,995,990,985,985,980,990,995,1000]
# foundTheories = least_squares_fit(fakeStock)

# print 'first parab plot'
# theory = foundTheories[5]
# theoryEvaluate = []
# for i in range(theory[3]):
    # print i 
    # functionValue = theory[0] + theory[1]*i + theory[2]*i*i
    # theoryEvaluate.append(functionValue)

# print 'sec parab plot'
# for i in range(1,theory[7]):
    # print i
    # functionValue = theory[4] + theory[5]*i + theory[6]*i*i
    # theoryEvaluate.append(functionValue)

# print 'third parab plot'
# for i in range(1,theory[11]):
    # print i
    # functionValue = theory[8] + theory[9]*i + theory[10]*i*i
    # theoryEvaluate.append(functionValue)
    
# plt.plot(theoryEvaluate)    
    
    
# plt.plot(fakeStock)
# plt.show()
# plt.savefig('results/fig1')
