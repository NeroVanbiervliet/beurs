from A_definitions import *
import numpy as np
import matplotlib.pyplot as plt
from leastsquarestestMatthias import *

bel20 = loadstocklist(1)
for j in range(len(bel20)):

    stock = bel20[j]
    stockpath = 'data/stockfiles/' + stock + '.txt'
    check = os.path.isfile(stockpath)
    
    if check:
        stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        stockfile = stockfile_normaliser(stockfile1)
        stockfile = np.multiply(stockfile,1000)
        
        # fitting
        theory = least_squares_fit(stockfile)

    
##        bestError = 100000.    
##        newError = 0.
##        theory = foundTheories[0]
##    
##        for theoryIterator in foundTheories:
##            newError = 0.
##            for q in range(theoryIterator[3]): # enkel error in eerste interval
##                newError += abs(stockfile[q]-(theoryIterator[0]+theoryIterator[1]*q+theoryIterator[2]*q*q))
##                
##            if newError < bestError:
##                theory =  theoryIterator
##                bestError = newError
##    
        theoryEvaluate = []
        
        # eerste deel van functie plotten
        for i in range(theory[3]):
            functionValue = theory[0] + theory[1]*i + theory[2]*i*i
            theoryEvaluate.append(functionValue)
        
        # tweede deel van functie plotten
        for i in range(1,theory[7]):
            functionValue = theory[4] + theory[5]*i + theory[6]*i*i
            theoryEvaluate.append(functionValue)
        
        # derde deel van functie plotten
        for i in range(1,theory[11]):
            functionValue = theory[8] + theory[9]*i + theory[10]*i*i
            theoryEvaluate.append(functionValue)
            
        plt.plot(theoryEvaluate)    
            
            
        plt.plot(stockfile[:(theory[3]+theory[7]+theory[11])])
        savePath = 'results/theory5_stock'+str(j)
        plt.savefig(savePath)
        plt.clf()
    else:
        warningMessage = 'Stock '+stock+' of bel20 not found in ' + stockpath
        print warningMessage
