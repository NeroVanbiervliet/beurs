from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import datetime
import pickle

def least_squares_fit(stock):
    
    theoryList = []

        
    for start in [1000,1020,980,1040,960,1060,940,1080,920]:
        for days1 in [5,7]:
            for days2 in [5,7]:
                for days3 in [5,7]:
                    # first parabole f(x) = a1 + b1x + c1x**2
                    a1 = start
                    cte1 = 0
                    cte2 = 0
                    r11 = 0 
                    r12 = 0
                    r22 = 0            
                    
                    for x_i in range(days1):                      
                        cte1 += x_i*stock[x_i] - a1*x_i # kan efficienter door dit te berekenen uit deze lus met sum(range(days1)) apart, en start maar 1 maal te vermenidgvuldigen met de tweede componetn
                        cte2 += x_i*x_i*stock[x_i] - a1*x_i*x_i
                        r11 += x_i*x_i
                        r12 += x_i*x_i*x_i
                        r22 += x_i*x_i*x_i*x_i
                    
                    c1 = (cte2 + r12/r11*cte1)/(r12*r12/r11+r22)
                    b1 = (cte1 -c1*r12)/r11
                    
                    start2 = a1 + b1*(days1-1) + c1*(days1-1)*(days1-1)
                    
                    # second parabole                   
                    
                    a2 = start2
                    cte1 = 0
                    cte2 = 0
                    r11 = 0 
                    r12 = 0
                    r22 = 0            
                    
                    for x_i in range(days2):
                        cte1 += x_i*stock[x_i+days1] - a2*x_i # kan efficienter door dit te berekenen uit deze lus met sum(range(days1)) apart, en start maar 1 maal te vermenidgvuldigen met de tweede componetn
                        cte2 += x_i*x_i*stock[x_i+days1] - a2*x_i*x_i
                        r11 += x_i*x_i
                        r12 += x_i*x_i*x_i
                        r22 += x_i*x_i*x_i*x_i
                    
                    c2 = (cte2 + r12/r11*cte1)/(r12*r12/r11+r22)
                    b2 = (cte1 -c2*r12)/r11
                    
                    start3 = a2 + b2*(days2-1) + c2*(days2-1)*(days2-1)                   

                    # third parabole                 
                    
                    a3 = start3
                    cte1 = 0
                    cte2 = 0
                    r11 = 0 
                    r12 = 0
                    r22 = 0            
                    
                    for x_i in range(days3):
                        cte1 += x_i*stock[days1+days2+x_i] - a3*x_i # kan efficienter door dit te berekenen uit deze lus met sum(range(days1)) apart, en start maar 1 maal te vermenidgvuldigen met de tweede componetn
                        cte2 += x_i*x_i*stock[days1+days2+x_i] - a3*x_i*x_i
                        r11 += x_i*x_i
                        r12 += x_i*x_i*x_i
                        r22 += x_i*x_i*x_i*x_i
                    
                    c3 = (cte2 + r12/r11*cte1)/(r12*r12/r11+r22)
                    b3 = (cte1 -c3*r12)/r11
                    
                    theoryList.append([a1,b1,c1,days1,a2,b2,c2,days2,a3,b3,c3,days3])
                
    return theoryList
    
# # testje
    
# fakeStock = [1000,1020,1035,1040,1045,1060,940,940,920,940,940,960,965,955,970,990,1005,995,990,985,985,980,990,995,1000]
# foundTheories = least_squares_fit(fakeStock)

# print 'first parab plot'
# theory = foundTheories[5]
# theoryEvaluate = []
# for i in range(theory[3]):
    # print i 
    # functionValue = theory[0] + theory[1]*i + theory[2]*i*i
    # theoryEvaluate.append(functionValue)

# print 'sec parab plot'
# for i in range(1,theory[7]):
    # print i
    # functionValue = theory[4] + theory[5]*i + theory[6]*i*i
    # theoryEvaluate.append(functionValue)

# print 'third parab plot'
# for i in range(1,theory[11]):
    # print i
    # functionValue = theory[8] + theory[9]*i + theory[10]*i*i
    # theoryEvaluate.append(functionValue)
    
# plt.plot(theoryEvaluate)    
    
    
# plt.plot(fakeStock)
# plt.show()
# plt.savefig('results/fig1')