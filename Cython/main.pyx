def errorCalculator(list stockfile,list theory,int end,int sumdays):
    cdef int i
    cdef int j
    cdef int daycount
    cdef int running
    cdef float error,theoryPiece1,theoryPiece2,theoryPiece3
	
    daycount = 0
    running = 1
    # eerste iteratie van i apart    
    i=0
    error = 0
    j=0 
    while j < theory[4*i+3] and running==1:       
        error = error + abs(stockfile[end + sumdays-1 -  j - daycount] - (theory[0]/1000. + theory[1]/1000.*(j) + theory[2]/1000.*(j)*(j))) 
        if error/theory[4*i+3] > 0.01:
            running = 0
        j += 1     
    daycount += theory[3]
    i += 1
    
    # alle volgende iteraties van i     
    while i < int(len(theory)/4) and running==1:    
        error = 0.     
        theoryPiece1 = theory[4*i]/1000.        
        theoryPiece2 = theory[4*i+1]/1000.        
        theoryPiece3 = theory[4*i+2]/1000.              
        j=0
        while j < theory[4*i+3] and running==1:       
            error = error + abs(stockfile[end + sumdays-1 -  j - daycount] - (theoryPiece1 + theoryPiece2*(j+1) + theoryPiece3*(j+1)*(j+1)))
            if error/theory[4*i+3] > 0.01:
                running = 0			
            j += 1
        daycount += theory[4*i+3]
        i += 1
        
    return running
