# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import datetime
import pickle
import time
from random import randint

cdef int errorCalculator(list stockfile,list theory,int end,int sumdays,float errormax):
    cdef int i
    cdef int j
    cdef int daycount
    cdef int running,redFactor
    cdef float error,theoryPiece1,theoryPiece2,theoryPiece3
	
    daycount = 0
    running = 1
    # eerste iteratie van i apart    
    i=0
    error = 0
    j=0
    redFactor = max(int(theory[4*i+3]/10),1)	
    while j < theory[4*i+3] and running==1:       
        error = error + abs(stockfile[end + sumdays-1 -  j] - (theory[0] + theory[1]*(j) + theory[2]*(j)*(j))) 
        if error/theory[4*i+3]*redFactor > errormax:
            running = 0
        j += 1*redFactor     
    daycount += theory[3]
    i += 1
    
    # alle volgende iteraties van i     
    while i < int(len(theory)/4) and running==1:
        redFactor = max(int(theory[4*i+3]/10),1)    
        error = 0.     
        theoryPiece1 = theory[4*i]       
        theoryPiece2 = theory[4*i+1]       
        theoryPiece3 = theory[4*i+2]             
        j=0
        while j < theory[4*i+3] and running==1:       
            error = error + abs(stockfile[end + sumdays-1 -  j - daycount] - (theoryPiece1 + theoryPiece2*(j+1) + theoryPiece3*(j+1)*(j+1)))
            if error/theory[4*i+3]*redFactor > errormax:
                running = 0			
            j += 1*redFactor
        daycount += theory[4*i+3]
        i += 1
        
    return running


def stockfile_normaliser(stockfile):

    cdef int i,b
    cdef float avg
    cdef list stockfile_corrected
    stockfile_corrected = []

    b = 240

    avg = sum(stockfile[:b])/b
    
    for i in xrange(len(stockfile)):

        stockfile_corrected.append(stockfile[i]/avg)

        if i+b+1 < len(stockfile):

            avg = avg - stockfile[i]/b + stockfile[i+b]/b

            # moving average

        
    return stockfile_corrected
	
def stockfile_smoother(stockfile):
    
    a = 5
    b = 3
    c = 1

    stockfile_smooth = []

    for i in xrange(len(stockfile)-3):
        
        stockfile_smooth.append((a*stockfile[i] + b*stockfile[i+1] + c*stockfile[i+2])/(a+b+c))
      

    return stockfile_smooth


def theoryFitNew(stockfile):
    x = range(0,300)

    theorylist = []
    
    for daysrange in [np.arange(5,12,2),np.arange(15,31,5),np.arange(35,56,5)]:
        
        errorBest = 999999
        
        
        for days1 in daysrange:
            for days2 in daysrange:
                for days3 in daysrange:
                    
                    x1 = x[:days1]
                    x2 = x[:days2+1]
                    x3 = x[:days3+1]

                    [c3,b3,a3],[R3],do,re,mi = np.polyfit(x1,stockfile[:days1][::-1],2,full=True)
                    [c2,b2,a2],[R2],do,re,mi = np.polyfit(x2,stockfile[days1-1:days2+days1][::-1],2,full=True)
                    [c1,b1,a1],[R1],do,re,mi = np.polyfit(x3,stockfile[days2+days1-1:days3+days1+days2][::-1],2,full=True)

                    error = R1/(days1**1.2) + R2/(days2**1.2) + R3/(days3**1.2)
                    
                    if error < errorBest:
                        if days1 < 13:
                            daysAfter = 20
                        if days1 > 13:
                            daysAfter = 70
                        if days1 > 34:
                            daysAfter = 150
                            
                        theory=[a1,b1,c1,days1,a2,b2,c2,days2,a3,b3,c3,days3,daysAfter]
                        errorBest=error
                        
        theorylist.append(theory)
    
    return theorylist

def loadstocklist(n):
    
    stocklist=np.loadtxt('data/stockfiles/stocklist.txt',dtype='string', delimiter=',', skiprows=0, usecols=(0,), unpack=False)

    if n==1:
        #BEL20
        stocklist=stocklist[:50]

    if n==2:
        #NASDAQ
        stocklist=stocklist[50:150]
        
    if n==3:
        #Dow jones
        stocklist=stocklist[150:179]
        
    if n==4:
        #FTSE
        stocklist=stocklist[179:203]
        
    if n==5:
        #AEX
        stocklist=stocklist[203:227]
        
    #['AGB.BR','AVH.BR','BEFB.BR','BEK.BR','BLG.BR','COFB.BR','COL.BR','DEL.BR','DIE.BR','DL.BR','ELI.BR','GBL.BR','GSZ.BR','INT.BR','KBC.BR','SOL.BR','THR.BR','TNO.BR','UCB.BR','UMC.BR']

    return stocklist

    ## Deze definitie gaat een lijst met aandelen laden, gewoon de naam ervan. Als de input 1 is, dan doet hij enkel de BEL20

cdef list resultAnalyser(list stockfile,int end,int days_after):
    cdef list results_array
    results_array = []
    
    for i in range(int(days_after)):
        results_array.append(max(min((stockfile[end-1-i] - stockfile[end])/(stockfile[end]),0.5),-0.5))
        
    return results_array  

cdef list historic(list stockfile,list theoryOriginal,int days_after):
    cdef int sumdays1,i,end,sumdays,count
    cdef list resultslist,theorylist,temp
    cdef float errormax,summation,dt1,dt2,dt3
    resultslist = []
    sumdays1 = 0
    end = days_after
    
    for i in range(int(len(theoryOriginal)/4)):
        sumdays1 = sumdays1 + theoryOriginal[4*i+3]
        
    theorylist = []
    for dt1 in [1,1.1,0.9]:
        for dt2 in [1,1.1,0.9]:
            for dt3 in [1,1.1,0.9]:

                temp = []
                sumdays = theoryOriginal[3]*dt1+theoryOriginal[7]*dt2+theoryOriginal[11]*dt3

                temp.append(theoryOriginal[0])
                temp.append(theoryOriginal[1]/dt1)
                temp.append(theoryOriginal[2]/(dt1*dt1))
                temp.append(theoryOriginal[3]*dt1)

                temp.append(theoryOriginal[4])
                temp.append(theoryOriginal[5]/dt2)
                temp.append(theoryOriginal[6]/(dt2*dt2))
                temp.append(theoryOriginal[7]*dt2)

                temp.append(theoryOriginal[8])
                temp.append(theoryOriginal[9]/dt3)
                temp.append(theoryOriginal[10]/(dt3*dt3))
                temp.append(theoryOriginal[11]*dt3)
                temp.append(sumdays)

                summation = 0.

                for i in range(theoryOriginal[3]):
                    summation += theoryOriginal[0] + theoryOriginal[1]*i + theoryOriginal[2]*i*i
                for i in range(theoryOriginal[7]):
                    summation += theoryOriginal[4] + theoryOriginal[5]*(i+1) + theoryOriginal[6]*(i+1)*(i+1)
                for i in range(theoryOriginal[11]):
                    summation += theoryOriginal[8] + theoryOriginal[9]*(i+1) + theoryOriginal[10]*(i+1)*(i+1)
                    
                temp.append(summation)
                
                theorylist.append(temp)

              
        
    while end < min(len(stockfile)-sumdays1*2-days_after,days_after*100):
        found = False
        count = 0
        
        while not found and count < len(theorylist):

            theory = theorylist[count] 

            sumdays = theory[12]
            errormax = 0.01*(1./60.*sumdays+0.7)
			
            count += 1
            if abs(sum(stockfile[end:end+sumdays]) - theory[13]) < errormax*sumdays:
                if errorCalculator(stockfile,theory,end,sumdays,errormax):
                    result = resultAnalyser(stockfile,end,days_after)
                    resultslist.append(result)
                    #plotTheoryVSStock(stockfile[end:],theory,errormax)
                    end += sumdays/3
                    found = True
            
        end += 1
    
    return resultslist


cdef predictor(list results,int days_after):
    cdef int amount
    cdef list days,expectations,probability
    
    days=[]
    amount = 0
    for i in range(days_after):
        days.append([])

    for i in range(len(results)):
        for j in range(len(results[i])):
            amount += 1
            for day in range(days_after):
                days[day].append(results[i][j][day])
                
    expectations = []
    probability = []
    if len(days[0]) > 1:
        for day in range(days_after):
            expectations.append(np.mean(days[day]))
            probability.append(1-st.norm.cdf(-np.mean(days[day])/np.std(days[day])))
        
    return expectations,probability,amount

def marketCheck(stock):
    market = 0
    stocklist = loadstocklist(0)
    for i in range(len(stocklist)):
        if stocklist[i] == stock:
            if i < 50:
                market = 1
                
            if i > 49 and i < 150:
                market = 2

            if i > 149 and i < 179:
                market = 3
                
            if i > 178 and i < 203:
                market = 4
                
            if i > 202 and i < 227:
                market = 5
                
    return market

def method1(stockfile,stock,entry,type):
    t = time.time()
    theorylist = theoryFitNew(stockfile)
    expectation, probability, amount = [],[],0
    results = []
    market = marketCheck(stock)    
    stocklist = loadstocklist(market)
    i = 0
    global types
    types = type
    global graphname
    graphname = 0    
    for stock in stocklist:
        
        stockpath = 'data/stockfiles/' + stock + '.txt'
        check=os.path.isfile(stockpath)
        if check:
            try:
                stockfile2 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                stockfile2 = stockfile_normaliser(stockfile2)
                stockfile2 = stockfile_smoother(stockfile2)
                
                if entry + 12*3 < len(stockfile2) and types == 'Short': # als stockfile langer is dan minstens 1 theory lengte                    
                    resultsTemp = historic(stockfile2[entry:],theorylist[0],theorylist[0][12])
                    results.append(resultsTemp)
                    i = 0
                    
                if entry + 35*3 < len(stockfile2) and types == 'Mid':                    
                    resultsTemp = historic(stockfile2[entry:],theorylist[1],theorylist[1][12])
                    results.append(resultsTemp)
                    i = 1
                    
                if entry + 80*3 < len(stockfile2) and types == 'Long':                    
                    resultsTemp = historic(stockfile2[entry:],theorylist[2],theorylist[2][12])
                    results.append(resultsTemp)
                    i = 2
                
            except ValueError:
                print stockpath + ' not available'
                          
    
    expectation, probability, amount = predictor(results,theorylist[i][12])
    print 'time:',(time.time()-t)          
    return expectation, probability, amount


def graphsaver(Packlist,title,name):
    plt.plot(Packlist)
    plt.title(title)
    plt.savefig('Simulations/' +name+ '.png')
    plt.close()

def resultsCheck():
    directory = 'data/results/' + str(datetime.datetime.now().date())
    check = os.path.isdir(directory)
    
    return check

    
def makeGraphResults(E,stockfile,stock):
    Enew = []
    for i in range(len(E)):
        Enew.append(stockfile[0]*(1+E[i]))
    days1 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
    days2 = [31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50]
    plt.figure(figsize=(6.5, 5.5))
    plt.plot(days1,stockfile[::-1][-30:])
    plt.plot(days2[:len(E)],Enew)
    u = np.mean(stockfile[::-1][-30:])
    plt.ylim([0.9*u,1.1*u])
    plt.title(stock)
    plt.savefig('images/graph'+stock+'.png')

def resultLoaderCombined(market):
    resultsFinal = []
    date = str(datetime.datetime.now().date())
    month = date[5:7]
    day = date[8:10]
    datelist = ['2014-10-11','2014-10-10','2014-10-09','2014-10-08','2014-10-07','2014-10-04','2014-10-03','2014-10-02','2014-10-01','2014-09-30','2014-09-29','2014-09-28','2014-09-27','2014-09-26','2014-09-25','2014-09-24','2014-09-23','2014-09-20','2014-09-19','2014-09-18','2014-09-17','2014-09-16','2014-09-13','2014-09-12','2014-09-11','2014-09-10','2014-09-09','2014-09-06','2014-09-05']
    i = 0
    count = 0
    while not i==20 and count < 20:
        datenew = datelist[count]
        count += 1   
        path = 'data/results/' + datenew + '/results' + str(market) + '.txt'
        check = os.path.isfile(path)
        if check:
            
            f = open(path,'r')
            result = pickle.load(f)
            f.close()
            for j in range(len(result)):
                stock = result[j][0]
                found = False
                for entry in range(len(resultsFinal)):
                    if resultsFinal[entry][0] == stock:
                        found = True
                        index = entry
                if found:
                    resultsFinal[index][1].append(result[j][1][:(20-i)])
                    resultsFinal[index][2].append(result[j][2][:(20-i)])
                    resultsFinal[index][3].append(result[j][3])
                    
                else:
                    resultsFinal.append([result[j][0],[result[j][1][:(20-i)]],[result[j][2][:(20-i)]],[result[j][3]]])
        
            i += 1
        
        
    if resultsFinal:
        for j in range(len(resultsFinal)): #alle stocks overlopen
            amountnew = 0
            if not len(resultsFinal[j][1]) == 1: #als er meerdere resultaten zijn
                
                Etemp = []
                Enew = []
                for i in range(len(resultsFinal[j][1])): #alle resultaten overlopen
                    for p in range(len(resultsFinal[j][1][i])):
                        if i == 0:
                            Etemp.append([])
                        Etemp[p].append(resultsFinal[j][1][i][p])
                        
                for i in range(len(Etemp)):
                    Enew.append(np.mean(Etemp[i]))
                
                resultsFinal[j][1] = Enew

                Ptemp = []
                Pnew = []
                for i in range(len(resultsFinal[j][2])):
                    for p in range(len(resultsFinal[j][2][i])):
                        if i == 0:
                            Ptemp.append([])
                        Ptemp[p].append(resultsFinal[j][2][i][p])
                        
                for i in range(len(Ptemp)):
                    Pnew.append(np.mean(Ptemp[i]))
                
                resultsFinal[j][2] = Pnew
                
                for i in range(len(resultsFinal[j][3])):
                    amountnew += resultsFinal[j][3][i]
                    
                amount = amountnew/len(resultsFinal[j][3])
                resultsFinal[j][3]= amount
                
            else:
                resultsFinal[j][1] = resultsFinal[j][1][0]
                resultsFinal[j][2] = resultsFinal[j][2][0]
                resultsFinal[j][3] = resultsFinal[j][3][0]
            
            
    return resultsFinal   
        
def find(value,list):
    
    for i in range(len(list)):
        if abs(value - list[i]) < 0.000001:
            
            entry = i
            
    return entry

def plotTheoryVSStock(stockfilepart,theory,errormax):
    theoryEvaluate = []
    global graphname
    global types
    graphname += 1
    # eerste deel van functie plotten
    for i in range(theory[3]):
        functionValue = theory[0] + theory[1]*i + theory[2]*i*i
        theoryEvaluate.append(functionValue)
    
    # tweede deel van functie plotten
    for i in range(1,theory[7]+1):
        functionValue = theory[4] + theory[5]*i + theory[6]*i*i
        theoryEvaluate.append(functionValue)
    
    # derde deel van functie plotten
    for i in range(1,theory[11]+1):
        functionValue = theory[8] + theory[9]*i + theory[10]*i*i
        theoryEvaluate.append(functionValue)
    plt.close()
    plt.plot(theoryEvaluate)
    plt.plot(stockfilepart[:(theory[3]+theory[7]+theory[11])][::-1])
    plt.title(str(errormax))
    plt.axis([0,len(theoryEvaluate)+1,0.6,1.4])
    #plt.show()
    savePath = 'data/results/tests/' + types + str(graphname)
    plt.savefig(savePath)
    
