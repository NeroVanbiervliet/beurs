import pygame as pg
from A_definitions import *
import datetime
import os
import pickle
from Tkinter import *
from GraphFunctions import *


name  = 'Matthias Baert'
playing = True

#intro scherm en update van files
intro()

## en dan begint het
scr = pg.display.set_mode((1024,768))
scherm = pg.image.load('images/main.png')
pg.init()
font1 = pg.font.SysFont("monospace", 30)
font2 = pg.font.SysFont("monospace", 15)
white = (255,255,255)
grey = (183,170,161)
green = (20,193,85)
mystocks = False
predictions = False
detail = False
firsttime = True
z = False
u = False
market = 0
scroll = 0
while playing:

    pg.event.pump()
    keys = pg.key.get_pressed()
    mousepoint=pg.mouse.get_pos()
    mousebuttons=pg.mouse.get_pressed()

    ## screen setup
    #achtergrond
    scr.blit(scherm,(0,0))
    #tekst onderaan
    label = font1.render(name, 1, white)
    scr.blit(label, (10, 730))
    label = font1.render('Stock Assistent V1.0', 1, white)
    scr.blit(label, (660, 730))
    label = font2.render('ESC to quit', 1, white)
    scr.blit(label, (920, 0))
    #verticale lijn
    pg.draw.lines(scr, grey,False,[(300,0),(300,730)],5)

    
    if button(scr,'Predictions', font1, white, grey, (50,80),  mousepoint,mousebuttons):
        predictions = True
        mystocks = False
        detail = False
        

    if button(scr,'My Stocks', font1, white, grey, (50,150),  mousepoint,mousebuttons):
        predictions = False
        mystocks = True
        detail = False
  
    if not predictions:
        market = 0
    else: 
        bypass = True
        #if not resultsCheck():
        if not bypass:
            label = font1.render('No predictions found, update?', 1, white)
            scr.blit(label, (350, 50))

            if button(scr,'Start', font1, white, grey, (600,90),  mousepoint,mousebuttons):
                os.startfile("method1run.py")
                             
        else:

            if button(scr,'BEL', font1, white, grey, (350,30),  mousepoint,mousebuttons):
                market = 1
                scroll = 0
            if button(scr,'FTSE', font1, white, grey, (450,30),  mousepoint,mousebuttons):
                market = 4
                scroll = 0
            if button(scr,'AEX', font1, white, grey, (550,30),  mousepoint,mousebuttons):
                market = 5
                scroll = 0
            if button(scr,'Dow Jones', font1, white, grey, (700,30),  mousepoint,mousebuttons):
                market = 3
                scroll = 0

            if button(scr,'Up', font2, white, grey, (350,740),  mousepoint,mousebuttons):
                if z:
                    z=False
                    scroll += 1
            else:
                z=True
                
            if button(scr,'Down', font2, white, grey, (450,740),  mousepoint,mousebuttons):
                if u:
                    u=False
                    scroll -= 1
            else:
                u=True
            
            stockdetail = showResults(scr,market,scroll,mousepoint,mousebuttons)

            if stockdetail:
                predictions = False
                detail = True
                firsttime = True

    if detail:
        showDetail(scr,stockdetail,firsttime)
        firsttime = False    
    

    #mogelijkheid om af te sluiten
    
    if keys[pg.K_ESCAPE]:
        playing = False
    
    pg.display.flip()
    
pg.image.save(scr,'lala.png')
pg.quit()
