from definitions import *
import numpy as np
import urllib2
import os
import os.path
import random
from random import randint
import time

Pack1 = 10000 #methode 1
Pack2 = 10000 #methode 2
Pack3 = 10000 #methode 3
Pack4 = 10000 #methode 4
Pack5 = 10000 #methode 5
Pack6 = 10000

Pack1temp = 0 #methode 1
Pack2temp = 0 #methode 2
Pack3temp = 0 #methode 3
Pack4temp = 0 #methode 4
Pack5temp = 0 #methode 5
Pack6temp = 0

cost1 = 7.25
cost2 = 9.75

bel20 = loadstocklist(1)
AEX = loadstocklist(5)
FTSE = loadstocklist(4)
stocklist = [bel20,AEX,FTSE]



Pack1list = []
Pack2list = []
Pack3list = []
Pack4list = []
Pack5list = []
Pack6list = []

minbuy = 2000


simname = 'Sim 7'
days = 0

while days < 20:
    tstart = time.time()
    days += 1
    print 'Days:', days

    Plist = []
    Elist = []
    results = []
    entry = randint(30,600)
    for m in range(len(stocklist)):
        for stock in stocklist[m]:
            stockpath = 'data/stockfiles/' + stock + '.txt'
            check = os.path.isfile(stockpath)
            if check:
                
                stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                if len(stockfile1) > 700:
                    stockfile = stockfile_normaliser(stockfile1)
                    
                    E,P,amount = method1special(stockfile[entry:],stock,entry)
                    
                    
                    if E and amount > 40:
                        print stock
                        print 'amount:',amount
                        print 'E:',max(E)
                        print 'P:',max(P)
                                         
                        for i in range(len(E)):
                            if abs(E[i] - max(E)) < 0.00001:
                                entry1 = i+1
                        #print 'entry1:',entry1
                        minimum = 1000
                        for i in range(entry1):
                            if E[i] < minimum:
                                entry11 = i
                                minimum = E[i]
                        #print 'entry11:',entry11
                        
                        for i in range(len(P)):
                            if abs(P[i] - max(P)) < 0.00001:
                                entry2 = i+1
                        #print 'entry2:',entry2
                        minimum = 1000
                        for i in range(entry2):
                            if P[i] < minimum:
                                entry21 = i
                                minimum = P[i]
                        #print 'entry21:',entry21
                        
                        print 'winst/verlies na entry2 dagen in %:', (stockfile[entry-entry2]-stockfile[entry])/stockfile[entry]*100

                        future = []
                        for i in range(21):
                            future.append(stockfile[entry-i])

                        results.append([entry1,entry11,entry2,entry21,future])
                        Plist.append(max(P))
                        Elist.append(max(E))
    
    ## Strategie 1
    # Gelijk verdelen, geen 3% stop, 60% benchmark
    print ' '
    buylist = []
    Plistsorted = sorted(Plist)
    interestingStocks = 0
    for i in range(min(len(Plistsorted),int(Pack1/minbuy))):
        if Plistsorted[-(i+1)] > 0.6:
            interestingStocks = i+1
            
    for j in range(interestingStocks):
        for i in range(len(Plist)):
            if abs(Plist[i]-Plistsorted[-(j+1)]) < 0.000001:
                gain = (results[i][4][results[i][2]]-results[i][4][0])/results[i][4][0]
                buylist.append([Plist[i],gain])
                print 'P:',Plist[i]
                print 'gain:', gain
    
    if buylist: 
        invest = min((Pack1-len(buylist)*cost2)/len(buylist),Pack1/4)
        totalgain = 0.
        totalcost = 0.
        for i in range(len(buylist)):
            totalgain += invest*buylist[i][1]
            if invest < 2500:
                totalcost += cost1
            else:
                totalcost += cost2
                
        Pack1 = Pack1 + totalgain - totalcost
        print 'Pack1 gain:', totalgain 

    ## Strategie 2
    # Gelijk verdelen, 3% stop, 60% benchmark
    print ' '
    buylist = []
    Plistsorted = sorted(Plist)
    interestingStocks = 0
    for i in range(min(len(Plistsorted),int(Pack2/minbuy))):
        if Plistsorted[-(i+1)] > 0.6:
            interestingStocks = i+1
    
    for j in range(interestingStocks):
        
        for i in range(len(Plist)):
            if abs(Plist[i]-Plistsorted[-(j+1)]) < 0.00001:
                stop = False
                gain = 0.
                for p in range(results[i][2]):
                    gaintemp = (results[i][4][p]-results[i][4][0])/results[i][4][0]
                    if gain > -0.03 and (not stop):
                        gain = gaintemp
                    else:
                        stop=True
                
                buylist.append([Plist[i],gain])
                print 'P:',Plist[i]
                print 'gain:', gain
                
    
    if buylist:
        invest = min((Pack2-len(buylist)*cost2)/len(buylist),Pack2/4)
        totalgain = 0.
        totalcost = 0.
        for i in range(len(buylist)):
            totalgain += invest*buylist[i][1]
            if invest < 2500:
                totalcost += cost1
            else:
                totalcost += cost2

        Pack2 = Pack2 + totalgain - totalcost
        print 'Pack2 gain:', totalgain

    ## Strategie 3
    # Gelijk verdelen, geen 3% stop, 65% benchmark
    print ' '
    buylist = []
    Plistsorted = sorted(Plist)
    interestingStocks = 0
    for i in range(min(len(Plistsorted),int(Pack3/minbuy))):
        if Plistsorted[-(i+1)] > 0.65:
            interestingStocks = i+1
       
    for j in range(interestingStocks):
        for i in range(len(Plist)):
            if abs(Plist[i]-Plistsorted[-(j+1)]) < 0.000001:
                gain = (results[i][4][results[i][2]]-results[i][4][0])/results[i][4][0]
                buylist.append([Plist[i],gain])
                print 'P:',Plist[i]
                print 'gain:', gain
                
    
    if buylist: 
        invest = min((Pack3-len(buylist)*cost2)/len(buylist),Pack3/4)
        totalgain = 0.
        totalcost = 0.
        for i in range(len(buylist)):
            totalgain += invest*buylist[i][1]
            if invest < 2500:
                totalcost += cost1
            else:
                totalcost += cost2
        Pack3 = Pack3 + totalgain - totalcost
        print 'Pack3 gain:', totalgain 

    ## Strategie 4
    # Gelijk verdelen, 3% stop, 65% benchmark
    print ' '
    buylist = []
    Plistsorted = sorted(Plist)
    interestingStocks = 0
    for i in range(min(len(Plistsorted),int(Pack4/minbuy))):
        if Plistsorted[-(i+1)] > 0.65:
            interestingStocks = i+1
          
    for j in range(interestingStocks):
         
        for i in range(len(Plist)):
            if abs(Plist[i]-Plistsorted[-(j+1)]) < 0.00001:
                stop = False
                gain = 0.
                for p in range(results[i][2]):
                    gaintemp = (results[i][4][p]-results[i][4][0])/results[i][4][0]
                    if gain > -0.03 and (not stop):
                        gain = gaintemp
                    else:
                        stop=True
                
                buylist.append([Plist[i],gain])
                print 'P:',Plist[i]
                print 'gain:', gain
                

    if buylist:
        invest = min((Pack4-len(buylist)*cost2)/len(buylist),Pack4/4)
        totalgain = 0.
        totalcost = 0.
        for i in range(len(buylist)):
            totalgain += invest*buylist[i][1]
            if invest < 2500:
                totalcost += cost1
            else:
                totalcost += cost2

        Pack4 = Pack4 + totalgain - totalcost
        print 'Pack4 gain:', totalgain

    ## Strategie 5
    # Verdelen volgens kans, 60% benchmark
    print ' '
    buylist = []
    Plistsorted = sorted(Plist)
    interestingStocks = 0
    totalgain= 0.
    for i in range(min(len(Plistsorted),int(Pack5/minbuy))):
        if Plistsorted[-(i+1)] > 0.60:
            interestingStocks = i+1

    investavg = (Pack5 - cost2*interestingStocks)/sum(Plistsorted[-interestingStocks:])
    
    for j in range(interestingStocks):
        for i in range(len(Plist)):
            if abs(Plist[i]-Plistsorted[-(j+1)]) < 0.000001:
                gain = (results[i][4][results[i][2]]-results[i][4][0])/results[i][4][0]
                if investavg*Plist[i]>2500:
                    cost = cost2
                else:
                    cost = cost1
                totalgain += min(investavg*Plist[i],Pack5/3)*gain
                Pack5 = Pack5 + min(investavg*Plist[i],Pack5/3)*gain - cost
                print 'P:',Plist[i]
                print 'invest:',min(investavg*Plist[i],Pack5/3)
                print 'gain:', gain
                
    print 'Pack5 gain:',totalgain

    ## Strategie 6
    # Verdelen volgens kans, 65% benchmark.
    print ' '
    buylist = []
    Plistsorted = sorted(Plist)
    interestingStocks = 0
    totalgain = 0.
    for i in range(min(len(Plistsorted),int(Pack6/minbuy))):
        if Plistsorted[-(i+1)] > 0.65:
            interestingStocks = i+1

    investavg = (Pack6 - cost2*interestingStocks)/sum(Plistsorted[-interestingStocks:])
    
    for j in range(interestingStocks):
        for i in range(len(Plist)):
            if abs(Plist[i]-Plistsorted[-(j+1)]) < 0.000001:
                gain = (results[i][4][results[i][2]]-results[i][4][0])/results[i][4][0]
                if investavg*Plist[i]>2500:
                    cost = cost2
                else:
                    cost = cost1
                totalgain += min(investavg*Plist[i],Pack6/3)*gain    
                Pack6 = Pack6 + min(investavg*Plist[i],Pack6/3)*gain - cost
                print 'P:',Plist[i]
                print 'invest:',min(investavg*Plist[i],Pack6/3)
                print 'gain:', gain
                
    print 'Pack6 gain:',totalgain
    
    Pack1list.append(Pack1)
    Pack2list.append(Pack2)
    Pack3list.append(Pack3)
    Pack4list.append(Pack4)
    Pack5list.append(Pack5)
    Pack6list.append(Pack6)
    print 'Pack1:',Pack1
    print 'Pack2:',Pack2
    print 'Pack3:',Pack3
    print 'Pack4:',Pack4
    print 'Pack5:',Pack5
    print 'Pack6:',Pack6
    print 'total time for one day:', (time.time()-tstart)
            

