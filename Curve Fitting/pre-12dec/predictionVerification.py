from definitions import *
import numpy as np
import urllib2
import os
import os.path
import random
from random import randint
import time
import matplotlib.pyplot as plt

bel20 = loadstocklist(1)
AEX = loadstocklist(5)
FTSE = loadstocklist(4)
stocklist = [bel20,AEX,FTSE]

count = 0

predictionList = []
realList = []

simname = 'Sim10'

while count < 300:
    m = randint(0,2)
    lala = randint(0,len(stocklist[m])-1)
    entry = randint(30,600)
    stock = stocklist[m][lala]
    stockpath = 'data/stockfiles/' + stock + '.txt'
    check = os.path.isfile(stockpath)
    if check:
        stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        stockfile = stockfile_normaliser(stockfile1)
        if len(stockfile1) > 700:
            E,P,amount = method1special_short(stockfile[entry:],stock,entry)
            if E and amount > 40:
                count += 1
                print count
                gain1 = (stockfile[entry-21]-stockfile[entry])/stockfile[entry]*100
                gain2 = (stockfile[entry-20]-stockfile[entry])/stockfile[entry]*100
                gain3 = (stockfile[entry-19]-stockfile[entry])/stockfile[entry]*100
                
                predictionList.append(E[19]*100)
                realList.append((gain1+gain2+gain3)/3)

[m,q] = np.polyfit(predictionList,realList,1)

plt.scatter(predictionList,realList)
plt.axis([-10,10,-10,10])
plt.xlabel('Geschat percentage winst')
plt.ylabel('Percentage winst')
plt.title('samples' + str(count) + 'm:' + str(round(m,2)) + 'q:' + str(round(q,2)))
savePath = 'data/results/' + simname
plt.savefig(savePath)
plt.show()

