# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import datetime
import pickle
import time

cdef int errorCalculator(stockfile,theory,int end,int sumdays,errormax):
    cdef int i
    cdef int j
    cdef int daycount
    cdef int running
    cdef float error,theoryPiece1,theoryPiece2,theoryPiece3
	
    daycount = 0
    running = 1
    # eerste iteratie van i apart    
    i=0
    error = 0
    j=0 
    while j < theory[4*i+3] and running==1:       
        error = error + abs(stockfile[end + sumdays-1 -  j - daycount] - (theory[0]/1000. + theory[1]/1000.*(j) + theory[2]/1000.*(j)*(j))) 
        if error/theory[4*i+3] > errormax:
            running = 0
        j += 1     
    daycount += theory[3]
    i += 1
    
    # alle volgende iteraties van i     
    while i < int(len(theory)/4) and running==1:    
        error = 0.     
        theoryPiece1 = theory[4*i]/1000.        
        theoryPiece2 = theory[4*i+1]/1000.        
        theoryPiece3 = theory[4*i+2]/1000.              
        j=0
        while j < theory[4*i+3] and running==1:       
            error = error + abs(stockfile[end + sumdays-1 -  j - daycount] - (theoryPiece1 + theoryPiece2*(j+1) + theoryPiece3*(j+1)*(j+1)))
            if error/theory[4*i+3] > errormax:
                running = 0			
            j += 1
        daycount += theory[4*i+3]
        i += 1
        
    return running


def stockfile_normaliser(stockfile):

    cdef int i,b
    cdef float avg
    cdef list stockfile_corrected
    stockfile_corrected = []

    b = 120

    avg = sum(stockfile[:b])/b
    
    for i in xrange(len(stockfile)):

        stockfile_corrected.append(stockfile[i]/avg)

        if i+b+1 < len(stockfile):

            avg = avg - stockfile[i]/b + stockfile[i+b]/b

            # moving average

        
    return stockfile_corrected

def theoryFitNew_short(stockfile):
    
    x = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35]
    errorBest = 999999
    
    for days1 in [5,7,10,12]:
        for days2 in [5,7,10,12]:
            for days3 in [5,7,10,12]:
    
                x1 = x[:days1]
                x2 = x[:days2+1]
                x3 = x[:days3+1]

                [c1,b1,a1],[R1],do,re,mi = np.polyfit(x1,stockfile[:days1],2,full=True)
                [c2,b2,a2],[R2],do,re,mi = np.polyfit(x2,stockfile[days1-1:days2+days1],2,full=True)
                [c3,b3,a3],[R3],do,re,mi = np.polyfit(x3,stockfile[days2+days1-1:days3+days1+days2],2,full=True)

                error = R1/(days1**1.3) + R2/(days2**1.3) + R3/(days3**1.3)
                
                if error < errorBest:
                    theory=[a1*1000,b1*1000,c1*1000,days1,a2*1000,b2*1000,c2*1000,days2,a3*1000,b3*1000,c3*1000,days3]
                    errorBest=error
                    
    return theory


def theoryFitNew_long(stockfile):
    
    x = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35]
    errorBest = 999999
    
    for days1 in [15,25,35]:
        for days2 in [15,25,35]:
            for days3 in [15,25,35]:
    
                x1 = x[:days1]
                x2 = x[:days2+1]
                x3 = x[:days3+1]

                [c1,b1,a1],[R1],do,re,mi = np.polyfit(x1,stockfile[:days1],2,full=True)
                [c2,b2,a2],[R2],do,re,mi = np.polyfit(x2,stockfile[days1-1:days2+days1],2,full=True)
                [c3,b3,a3],[R3],do,re,mi = np.polyfit(x3,stockfile[days2+days1-1:days3+days1+days2],2,full=True)

                error = R1/(days1**1.3) + R2/(days2**1.3) + R3/(days3**1.3)
                
                if error < errorBest:
                    theory=[a1*1000,b1*1000,c1*1000,days1,a2*1000,b2*1000,c2*1000,days2,a3*1000,b3*1000,c3*1000,days3]
                    errorBest=error
                    
    return theory


def loadstocklist(n):
    
    stocklist=np.loadtxt('data/stockfiles/stocklist.txt',dtype='string', delimiter=',', skiprows=0, usecols=(0,), unpack=False)

    if n==1:
        #BEL20
        stocklist=stocklist[:50]

    if n==2:
        #NASDAQ
        stocklist=stocklist[50:150]
        
    if n==3:
        #Dow jones
        stocklist=stocklist[150:179]
        
    if n==4:
        #FTSE
        stocklist=stocklist[179:203]
        
    if n==5:
        #AEX
        stocklist=stocklist[203:227]
        
    #['AGB.BR','AVH.BR','BEFB.BR','BEK.BR','BLG.BR','COFB.BR','COL.BR','DEL.BR','DIE.BR','DL.BR','ELI.BR','GBL.BR','GSZ.BR','INT.BR','KBC.BR','SOL.BR','THR.BR','TNO.BR','UCB.BR','UMC.BR']

    return stocklist

    ## Deze definitie gaat een lijst met aandelen laden, gewoon de naam ervan. Als de input 1 is, dan doet hij enkel de BEL20

def resultAnalyser(stockfile,end,days_after):

    results_array = []
    
    for i in range(int(days_after)):
        results_array.append(max(min((stockfile[end-1-i] - stockfile[end])/(stockfile[end]),0.5),-0.5))
        
    return results_array  

def historic(stockfile,theory,days_after):
    cdef int sumdays,i,end,end2
    cdef list resultslist
    sumdays = 0
    resultslist = []
    
    for i in range(int(len(theory)/4)):
        sumdays = sumdays + theory[4*i+3]
    errormax = 0.01*sumdays**(0.3)

    for end in xrange(min(len(stockfile)-sumdays-days_after,1000)):
        end2 = end + days_after
                    
        if errorCalculator(stockfile,theory,end2,sumdays,errormax):
            result = resultAnalyser(stockfile,end2,days_after)
            resultslist.append(result)
    
    return resultslist


def predictor(results,days_after):

    days=[]
    amount = 0
    for i in range(days_after):
        days.append([])

    for i in range(len(results)):
        for j in range(len(results[i])):
            for day in range(days_after):
                amount += 1
                days[day].append(results[i][j][day])
                
    expectations = []
    probability = []
    if days[0]:
        for day in range(days_after):
            expectations.append(np.mean(days[day]))
            probability.append(1-st.norm.cdf(-np.mean(days[day])/np.std(days[day])))
        
    return expectations,probability,amount

def marketCheck(stock):
    market = 0
    stocklist = loadstocklist(0)
    for i in range(len(stocklist)):
        if stocklist[i] == stock:
            if i < 50:
                market = 1
                
            if i > 49 and i < 150:
                market = 2

            if i > 149 and i < 179:
                market = 3
                
            if i > 178 and i < 203:
                market = 4
                
            if i > 202 and i < 227:
                market = 5
                
    return market

def method1special_short(stockfile,stock,entry):
    #t = time.time()
    #theorylist = theoryGenerator()
    theory = theoryFitNew_short(stockfile)
    expectation = []
    probability = []
    amount = 0
    results = []
    market = marketCheck(stock)
    days_after = 20
    
    stocklist = loadstocklist(market)

    for stock in stocklist:
        
        stockpath = 'data/stockfiles/' + stock + '.txt'
        check=os.path.isfile(stockpath)
        if check:
            try:
                stockfile2 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                stockfile2 = stockfile_normaliser(stockfile2)
                if entry+30 < len(stockfile2):

                    resultsTemp = historic(stockfile2[entry:],theory,days_after)
                    results.append(resultsTemp)
                
            except ValueError:
                print stockpath + ' not available'
                          
    
    expectation, probability, amount = predictor(results,days_after)
    #amount = amount/len(theoryfit)
    #print 'time:',(time.time()-t)          
    return expectation, probability, amount

def method1special_long(stockfile,stock,entry):
    #t = time.time()
    #theorylist = theoryGenerator()
    theory = theoryFitNew_long(stockfile)
    expectation = []
    probability = []
    amount = 0
    results = []
    market = marketCheck(stock)
    days_after = 80
    
    stocklist = loadstocklist(market)

    for stock in stocklist:
        
        stockpath = 'data/stockfiles/' + stock + '.txt'
        check=os.path.isfile(stockpath)
        if check:
            try:
                stockfile2 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                stockfile2 = stockfile_normaliser(stockfile2)
                if entry+30 < len(stockfile2):

                    resultsTemp = historic(stockfile2[entry:],theory,days_after)
                    results.append(resultsTemp)
                
            except ValueError:
                print stockpath + ' not available'
                          
    
    expectation, probability, amount = predictor(results,days_after)
    #amount = amount/len(theoryfit)
    #print 'time:',(time.time()-t)          
    return expectation, probability, amount	
	
def graphsaver(Packlist,title,name):
    plt.plot(Packlist)
    plt.title(title)
    plt.savefig('Simulations/' +name+ '.png')
    plt.close()

def resultsCheck():
    directory = 'data/results/' + str(datetime.datetime.now().date())
    check = os.path.isdir(directory)
    
    return check

    
def makeGraphResults(E,stockfile,stock):
    Enew = []
    for i in range(len(E)):
        Enew.append(stockfile[0]*(1+E[i]))
    days1 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
    days2 = [31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50]
    plt.figure(figsize=(6.5, 5.5))
    plt.plot(days1,stockfile[::-1][-30:])
    plt.plot(days2[:len(E)],Enew)
    u = np.mean(stockfile[::-1][-30:])
    plt.ylim([0.9*u,1.1*u])
    plt.title(stock)
    plt.savefig('images/graph'+stock+'.png')

def resultLoaderCombined(market):
    resultsFinal = []
    date = str(datetime.datetime.now().date())
    month = date[5:7]
    day = date[8:10]
    datelist = ['2014-10-11','2014-10-10','2014-10-09','2014-10-08','2014-10-07','2014-10-04','2014-10-03','2014-10-02','2014-10-01','2014-09-30','2014-09-29','2014-09-28','2014-09-27','2014-09-26','2014-09-25','2014-09-24','2014-09-23','2014-09-20','2014-09-19','2014-09-18','2014-09-17','2014-09-16','2014-09-13','2014-09-12','2014-09-11','2014-09-10','2014-09-09','2014-09-06','2014-09-05']
    i = 0
    count = 0
    while not i==20 and count < 20:
        datenew = datelist[count]
        count += 1   
        path = 'data/results/' + datenew + '/results' + str(market) + '.txt'
        check = os.path.isfile(path)
        if check:
            
            f = open(path,'r')
            result = pickle.load(f)
            f.close()
            for j in range(len(result)):
                stock = result[j][0]
                found = False
                for entry in range(len(resultsFinal)):
                    if resultsFinal[entry][0] == stock:
                        found = True
                        index = entry
                if found:
                    resultsFinal[index][1].append(result[j][1][:(20-i)])
                    resultsFinal[index][2].append(result[j][2][:(20-i)])
                    resultsFinal[index][3].append(result[j][3])
                    
                else:
                    resultsFinal.append([result[j][0],[result[j][1][:(20-i)]],[result[j][2][:(20-i)]],[result[j][3]]])
        
            i += 1
        
        
    if resultsFinal:
        for j in range(len(resultsFinal)): #alle stocks overlopen
            amountnew = 0
            if not len(resultsFinal[j][1]) == 1: #als er meerdere resultaten zijn
                
                Etemp = []
                Enew = []
                for i in range(len(resultsFinal[j][1])): #alle resultaten overlopen
                    for p in range(len(resultsFinal[j][1][i])):
                        if i == 0:
                            Etemp.append([])
                        Etemp[p].append(resultsFinal[j][1][i][p])
                        
                for i in range(len(Etemp)):
                    Enew.append(np.mean(Etemp[i]))
                
                resultsFinal[j][1] = Enew

                Ptemp = []
                Pnew = []
                for i in range(len(resultsFinal[j][2])):
                    for p in range(len(resultsFinal[j][2][i])):
                        if i == 0:
                            Ptemp.append([])
                        Ptemp[p].append(resultsFinal[j][2][i][p])
                        
                for i in range(len(Ptemp)):
                    Pnew.append(np.mean(Ptemp[i]))
                
                resultsFinal[j][2] = Pnew
                
                for i in range(len(resultsFinal[j][3])):
                    amountnew += resultsFinal[j][3][i]
                    
                amount = amountnew/len(resultsFinal[j][3])
                resultsFinal[j][3]= amount
                
            else:
                resultsFinal[j][1] = resultsFinal[j][1][0]
                resultsFinal[j][2] = resultsFinal[j][2][0]
                resultsFinal[j][3] = resultsFinal[j][3][0]
            
            
    return resultsFinal   
        
def find(value,list):
    found = False
    for i in range(len(list)):
        if value == list[i]:
            found = True
            
    return found
