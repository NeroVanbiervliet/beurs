from definitions import *
import numpy as np
import urllib2
import os
import os.path
import random
from random import randint
import time
import matplotlib.pyplot as plt
import pickle

bel20 = loadstocklist(1)
AEX = loadstocklist(5)
FTSE = loadstocklist(4)
stocklist = [bel20,AEX,FTSE]

count = 0

EpredictionListShort,EpredictionListMid,EpredictionListLong = [],[],[]
ErealListShort,ErealListMid,ErealListLong = [],[],[]
PpredictionListShort,PpredictionListMid,PpredictionListLong = [],[],[]
PrealListShort,PrealListMid,PrealListLong = [],[],[]

simname = 'Sim9'

while min(len(ErealListShort),len(ErealListMid),len(ErealListLong)) < 100 and count < 200:
#while min(len(ErealListShort),len(ErealListMid)) < 100 and count < 500:
 
    m = randint(0,2)
    lala = randint(0,len(stocklist[m])-1)
    entry = randint(0,600)
    stock = stocklist[m][lala]
    stockpath = 'data/stockfiles/' + stock + '.txt'
    check = os.path.isfile(stockpath)
    if check:
        stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        
        if len(stockfile1) > 1000:
            stockfileShort = stockfile_normaliser(stockfile1,120)
            Eshort,Pshort,amountshort = method1(stockfileShort[entry],stock,entry,'Short')
            stockfileMid = stockfile_normaliser(stockfile1,240)
            Emid,Pmid,amountmid = method1(stockfileMid[entry],stock,entry,'Mid')
            stockfileLong = stockfile_normaliser(stockfile1,360)
            Elong,Plong,amountlong = method1(stockfileLong[entry],stock,entry,'Long')
 
            if amountshort > 10:
                count += 1
                print 'Short:', len(ErealListShort)+1
                
                Eentry = find(max(Eshort),Eshort)
                gain1 = (stockfile1[entry-Eentry+1]-stockfile1[entry])/stockfile1[entry]*100
                gain2 = (stockfile1[entry-Eentry]-stockfile1[entry])/stockfile1[entry]*100
                if abs((gain1+gain2)/2) < 10:
                    
                    EpredictionListShort.append(Eshort[Eentry]*100)
                    ErealListShort.append((gain1+gain2)/2)

                Pentry = find(max(Pshort),Pshort)
                gain1 = (stockfile1[entry-Pentry+1]-stockfile1[entry])/stockfile1[entry]*100
                gain2 = (stockfile1[entry-Pentry]-stockfile1[entry])/stockfile1[entry]*100
                if abs((gain1+gain2)/2) < 10:
                    PpredictionListShort.append(Pshort[Pentry])
                    PrealListShort.append((gain1+gain2)/2)
                
            if amountmid > 10:
                print 'Mid:', len(ErealListMid)+1
                
                Eentry = find(max(Emid),Emid)
                gain1 = (stockfile1[entry-Eentry+1]-stockfile1[entry])/stockfile1[entry]*100
                gain2 = (stockfile1[entry-Eentry]-stockfile1[entry])/stockfile1[entry]*100
                if abs((gain1+gain2)/2) < 20:        
                    EpredictionListMid.append(Emid[Eentry]*100)
                    ErealListMid.append((gain1+gain2)/2)

                Pentry = find(max(Pmid),Pmid)
                gain1 = (stockfile1[entry-Pentry+1]-stockfile1[entry])/stockfile1[entry]*100
                gain2 = (stockfile1[entry-Pentry]-stockfile1[entry])/stockfile1[entry]*100
                if abs((gain1+gain2)/2) < 20:
                    PpredictionListMid.append(Pmid[Pentry])
                    PrealListMid.append((gain1+gain2)/2)
                
            if amountlong > 10:
                print 'Long:', len(ErealListLong)+1
                
                Eentry = find(max(Elong),Elong)
                gain1 = (stockfile1[entry-Eentry+1]-stockfile1[entry])/stockfile1[entry]*100
                gain2 = (stockfile1[entry-Eentry]-stockfile1[entry])/stockfile1[entry]*100
                if abs((gain1+gain2)/2) < 30:               
                    EpredictionListLong.append(Elong[Eentry]*100)
                    ErealListLong.append((gain1+gain2)/2)

                Pentry = find(max(Plong),Plong)
                gain1 = (stockfile1[entry-Pentry+1]-stockfile1[entry])/stockfile1[entry]*100
                gain2 = (stockfile1[entry-Pentry]-stockfile1[entry])/stockfile1[entry]*100
                if abs((gain1+gain2)/2) < 30:
                    PpredictionListLong.append(Plong[Pentry])
                    PrealListLong.append((gain1+gain2)/2)



results = [[],[],[],[],[],[],[],[],[],[]]
for i in range(len(EpredictionListShort)):
    for j in range(10):
        if EpredictionListShort[i] > j*1. and EpredictionListShort[i] < 1. + j*1.:
            results[j].append(ErealListShort[i])

for i in range(10):
    print 'Eshort ' + str(i*1.) + ' tot ' +str(1. + i*1.) , np.mean(results[i]),'amount:',len(results[i])

results = [[],[],[],[],[],[],[],[],[],[]]
for i in range(len(EpredictionListMid)):
    for j in range(10):
        if EpredictionListMid[i] > j*1. and EpredictionListMid[i] < 1. + j*1.:
            results[j].append(ErealListMid[i])

for i in range(10):
    print 'Emid ' + str(i*1.) + ' tot ' +str(1. + i*1.) , np.mean(results[i]),'amount:',len(results[i])

results = [[],[],[],[],[],[],[],[],[],[]]
for i in range(len(EpredictionListLong)):
    for j in range(10):
        if EpredictionListLong[i] > j*1. and EpredictionListLong[i] < 1. + j*1.:
            results[j].append(ErealListLong[i])

for i in range(10):
    print 'Elong ' + str(i*1.) + ' tot ' +str(1. + i*1.) , np.mean(results[i]),'amount:',len(results[i])

results = [[],[],[],[],[]]
for i in range(len(PpredictionListShort)):
    for j in range(5):
        if PpredictionListShort[i] > 0.5 + j*0.1 and PpredictionListShort[i] < 0.6 + j*0.1:
            results[j].append(PrealListShort[i])            

for i in range(5):
    counter = 0.
    for j in range(len(results[i])):
        if results[i][j] > 0:
            counter += 1.
        
    print 'Pshort ' + str(0.5+i*0.1) + ' tot ' +str(0.6+i*0.1) , counter/max(len(results[i]),1),'amount:',len(results[i])

results = [[],[],[],[],[]]
for i in range(len(PpredictionListMid)):
    for j in range(5):
        if PpredictionListMid[i] > 0.5 + j*0.1 and PpredictionListMid[i] < 0.6 + j*0.1:
            results[j].append(PrealListMid[i])            

for i in range(5):
    counter = 0.
    for j in range(len(results[i])):
        if results[i][j] > 0:
            counter += 1.
        
    print 'Pmid ' + str(0.5+i*0.1) + ' tot ' +str(0.6+i*0.1) , counter/max(len(results[i]),1),'amount:',len(results[i])

results = [[],[],[],[],[]]
for i in range(len(PpredictionListLong)):
    for j in range(5):
        if PpredictionListLong[i] > 0.5 + j*0.1 and PpredictionListLong[i] < 0.6 + j*0.1:
            results[j].append(PrealListLong[i])            

for i in range(5):
    counter = 0.
    for j in range(len(results[i])):
        if results[i][j] > 0:
            counter += 1.
        
    print 'PLong ' + str(0.5+i*0.1) + ' tot ' +str(0.6+i*0.1) , counter/max(len(results[i]),1),'amount:',len(results[i])    

                
[m,q] = np.polyfit(EpredictionListShort,ErealListShort,1)
plt.subplot(3,2,1)
plt.scatter(EpredictionListShort,ErealListShort)
plt.axis([-10,10,-15,15])
plt.xlabel('Geschat percentage winst')
plt.ylabel('Percentage winst')
plt.title('samples' + str(len(ErealListShort)) + ' m:' + str(round(m,2)) + ' q:' + str(round(q,2)) + ' Short')
plt.grid()

[m,q] = np.polyfit(EpredictionListMid,ErealListMid,1)
plt.subplot(3,2,3)
plt.scatter(EpredictionListMid,ErealListMid)
plt.axis([-15,15,-20,20])
plt.xlabel('Geschat percentage winst')
plt.ylabel('Percentage winst')
plt.title('samples' + str(len(ErealListMid)) + ' m:' + str(round(m,2)) + ' q:' + str(round(q,2))+ ' Mid')
plt.grid()

[m,q] = np.polyfit(EpredictionListLong,ErealListLong,1)
plt.subplot(3,2,5)
plt.scatter(EpredictionListLong,ErealListLong)
plt.axis([-15,15,-20,20])
plt.xlabel('Geschat percentage winst')
plt.ylabel('Percentage winst')
plt.title('samples' + str(len(ErealListLong)) + ' m:' + str(round(m,2)) + ' q:' + str(round(q,2))+ ' Long')
plt.grid()

[m,q] = np.polyfit(PpredictionListShort,PrealListShort,1)
plt.subplot(3,2,2)
plt.scatter(PpredictionListShort,PrealListShort)
plt.axis([0,1,-15,15])
plt.xlabel('Geschat kans winst')
plt.ylabel('Percentage winst')
plt.title('samples' + str(len(ErealListShort)) + ' m:' + str(round(m,2)) + ' q:' + str(round(q,2)) + ' Short')
plt.grid()

[m,q] = np.polyfit(PpredictionListMid,PrealListMid,1)
plt.subplot(3,2,4)
plt.scatter(PpredictionListMid,PrealListMid)
plt.axis([0,1,-20,20])
plt.xlabel('Geschat kans winst')
plt.ylabel('Percentage winst')
plt.title('samples' + str(len(ErealListMid)) + ' m:' + str(round(m,2)) + ' q:' + str(round(q,2))+ ' Mid')
plt.grid()

[m,q] = np.polyfit(PpredictionListLong,PrealListLong,1)
plt.subplot(3,2,6)
plt.scatter(PpredictionListLong,PrealListLong)
plt.axis([0,1,-20,20])
plt.xlabel('Geschat kans winst')
plt.ylabel('Percentage winst')
plt.title('samples' + str(len(ErealListLong)) + ' m:' + str(round(m,2)) + ' q:' + str(round(q,2))+ ' Long')
plt.grid()

savePath = 'data/results/' + simname
plt.savefig(savePath)
plt.show()

path = 'data/results/' + simname + '.txt'
f = open(path,'a+')
pickle.dump([EpredictionListShort,EpredictionListMid,EpredictionListLong,ErealListShort,ErealListMid,ErealListLong,PpredictionListShort,PpredictionListMid,PpredictionListLong,PrealListShort,PrealListMid,PrealListLong],f)
f.close()
