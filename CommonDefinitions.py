# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import datetime
import pickle
import time
from random import randint

def loadStockDataClose(stock):
    
    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        stockFile = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        return stockFile
    else:
        return []
    
def stockfile_normaliser(stockfile,length):
    
    stockfile_normalised = []

    for entry in xrange(len(stockfile)-length):
        dummy = []
        avg = np.mean(stockfile[entry:entry+length])
        for i in xrange(length):
            dummy.append(stockfile[entry+i]/avg)
        stockfile_normalised.append(dummy)
         
    return stockfile_normalised

def stockfile_normaliser2(stockfile,length,days):
    
    stockfile_normalised = []

    for entry in range(min(len(stockfile)-length,days)):
        dummy = []
        avg = np.mean(stockfile[entry:entry+length])
        for i in xrange(length):
            dummy.append(stockfile[entry+i]/avg)
        stockfile_normalised.append(dummy)
         
    return stockfile_normalised  

def stockfile_smoother(stockfile):
    
    a = 5
    b = 3
    c = 1

    stockfile_smooth = []

    for i in xrange(len(stockfile)-3):
        
        stockfile_smooth.append((a*stockfile[i] + b*stockfile[i+1] + c*stockfile[i+2])/(a+b+c))
      

    return stockfile_smooth

def loadstocklist(n):
    
    stocklist=np.loadtxt('../data/stockfiles/stocklist.txt',dtype='string', delimiter=',', skiprows=0, usecols=(0,), unpack=False)

    if n==1:
        #BEL
        stocklist=stocklist[:50]

    if n==2:
        #NASDAQ
        stocklist=stocklist[50:150]
        
    if n==3:
        #Dow jones
        stocklist=stocklist[150:179]
        
    if n==4:
        #FTSE
        stocklist=stocklist[179:203]
        
    if n==5:
        #AEX
        stocklist=stocklist[203:227]
        
    #['AGB.BR','AVH.BR','BEFB.BR','BEK.BR','BLG.BR','COFB.BR','COL.BR','DEL.BR','DIE.BR','DL.BR','ELI.BR','GBL.BR','GSZ.BR','INT.BR','KBC.BR','SOL.BR','THR.BR','TNO.BR','UCB.BR','UMC.BR']

    return stocklist

    ## Deze definitie gaat een lijst met aandelen laden, gewoon de naam ervan. Als de input 1 is, dan doet hij enkel de BEL20

def find(value,list):
    
    for i in range(len(list)):
        if abs(value - list[i]) < 0.000001:
            
            entry = i
            
    return entry

def marketCheck(stock):
    market = 0
    stocklist = loadstocklist(0)
    for i in range(len(stocklist)):
        if stocklist[i] == stock:
            if i < 50:
                market = 1
                
            if i > 49 and i < 150:
                market = 2

            if i > 149 and i < 179:
                market = 3
                
            if i > 178 and i < 203:
                market = 4
                
            if i > 202 and i < 227:
                market = 5
                
    return market

