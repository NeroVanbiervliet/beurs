# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
from random import randint
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *



def OBVCalculator(stock,entry,length):

    OBV = []
    previousOBV = 0

    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        stockVolume = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(5,), unpack=False)

        for j in range(min(len(stockClose)-entry-1,length)):
            
            i = min(len(stockClose)-entry-1,length) + entry -1 -j
            
            if stockClose[i] > stockClose[i+1]:
                OBVValue = previousOBV + stockVolume[i]
            if stockClose[i] < stockClose[i+1]:
                OBVValue = previousOBV - stockVolume[i]
            if stockClose[i] == stockClose[i+1]:
                OBVValue = previousOBV
                
            OBV.append(OBVValue)
            previousOBV = OBVValue

    return OBV[::-1]

        
    
