import sys
sys.path.insert(0, '../')
from CorrelationDefinitions import CorrelationCalculator,CorrelationMethod
from CommonDefinitions import *
import random
import matplotlib.pyplot as plt
import scipy.stats as st
import numpy as np

stockList = loadstocklist(0)
period = 20

EList = []
resultList = []
ricoList = []
count = 0
corrLimit = 0.7

while count < 500: 
    
    stock = stockList[random.randint(0,len(stockList)-1)]
    entry = random.randint(100,1000)
    
    E = CorrelationMethod(stock,entry,corrLimit)

    if E:
        if abs(E) > 0.00:
            count +=1
            print count
            
            stockPath = '../data/stockfiles/' + stock + '.txt'
            stockFile = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                   
            result1 = (stockFile[entry-period]-stockFile[entry])/stockFile[entry]
            result2 = (stockFile[entry-period-1]-stockFile[entry])/stockFile[entry]
            result3 = (stockFile[entry-period+1]-stockFile[entry])/stockFile[entry]

            stockPercentage = []
            for i in range(period):
                stockPercentage.append((stockFile[entry-i-1] - stockFile[entry-i])/stockFile[entry-i])

            rico = np.mean(stockPercentage)
            ricoList.append(rico)

            result = np.mean([result1,result2,result3])
            EList.append(E)
            resultList.append(result)
       
plt.close()
plt.subplot(2,1,1)
plt.scatter(EList,resultList,color='red')
plt.grid()
plt.subplot(2,1,2)
plt.scatter(EList,ricoList,color='blue')
plt.grid()

plt.show()


        
        
        
        
        


