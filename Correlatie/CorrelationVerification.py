import sys
sys.path.insert(0, '../')
from CorrelationDefinitions import CorrelationCalculator
from CommonDefinitions import *
import random
import matplotlib.pyplot as plt
import scipy.stats as st
import numpy as np

stockList = loadstocklist(0)

shortPeriod = 20
midPeriod = 40
longPeriod = 60

avgPeriod = 8

xlist = []

results = []
count =0
prevcount = 0

corrLimit = 0.8

while count < 100:

    if not count==prevcount:
        print count
        prevcount=count

    stock1 = stockList[random.randint(0,len(stockList)-1)]
    stock2 = stockList[random.randint(0,len(stockList)-1)]
    entry = random.randint(100,1000)

    if not stock1==stock2:

        corr = CorrelationCalculator(stock1,stock2,entry,500)
        

        if corr and abs(corr) > corrLimit:

            stockPath1 = '../data/stockfiles/' + stock1 + '.txt'
            stockFile1 = np.loadtxt(stockPath1, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
            stockPath2 = '../data/stockfiles/' + stock2 + '.txt'
            stockFile2 = np.loadtxt(stockPath2, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
         

            stock1Percentage = []
            for i in range(avgPeriod):
                stock1Percentage.append((stockFile1[entry+i] - stockFile1[entry+i+1])/stockFile1[entry+i+1])
                    
            stock2Percentage = []
            for i in range(avgPeriod):
                stock2Percentage.append((stockFile2[entry+i] - stockFile2[entry+i+1])/stockFile2[entry+i+1])
            
            if corr > corrLimit:

                if abs(np.mean(stock1Percentage)) < 0.01 and abs(np.mean(stock2Percentage)) > 0.01:
                    rico = np.mean(stock2Percentage)
                    count +=1
                    xlist.append(rico)
                    results.append((stockFile1[entry-shortPeriod] - stockFile1[entry])/stockFile1[entry])

                if abs(np.mean(stock1Percentage)) > 0.01 and abs(np.mean(stock2Percentage)) < 0.01:
                    rico = np.mean(stock1Percentage)
                    count +=1
                    xlist.append(rico)
                    results.append((stockFile2[entry-shortPeriod] - stockFile2[entry])/stockFile2[entry])

            if corr < corrLimit:

                if abs(np.mean(stock1Percentage)) < 0.01 and abs(np.mean(stock2Percentage)) > 0.01:
                    rico = -np.mean(stock2Percentage)
                    count +=1
                    xlist.append(rico)
                    results.append((stockFile1[entry-shortPeriod] - stockFile1[entry])/stockFile1[entry])

                if abs(np.mean(stock1Percentage)) > 0.01 and abs(np.mean(stock2Percentage)) < 0.01:
                    rico = -np.mean(stock1Percentage)
                    count +=1
                    xlist.append(rico)
                    results.append((stockFile2[entry-shortPeriod] - stockFile2[entry])/stockFile2[entry])
                 

plt.scatter(xlist,results)
plt.show()

