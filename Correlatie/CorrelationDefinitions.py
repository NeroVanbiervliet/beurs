# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
from random import randint
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *

def CorrelationMethod(stock,entry,corrLimit):

    avgPeriod = 10

    results = []

    stockList = loadstocklist(1)

    for stock2 in stockList:
        corr = CorrelationCalculator(stock,stock2,entry,500)

        if corr:
            if abs(corr) > corrLimit and not stock==stock2:
                #print stock2
                stockPath2 = '../data/stockfiles/' + stock2 + '.txt'
                stockFile2 = np.loadtxt(stockPath2, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                
                stock2Percentage = []
                for i in range(avgPeriod):
                    stock2Percentage.append((stockFile2[entry+i] - stockFile2[entry+i+1])/stockFile2[entry+i+1])
                    
                rico = np.mean(stock2Percentage)
                results.append(corr/abs(corr)*rico)

    if results:
        result = np.mean(results)
        #print len(results)

        return result
    else:
        return []
    

def CorrelationCalculator(stock1,stock2,entry,period):

    corr = []
    
    stockPath1 = '../data/stockfiles/' + stock1 + '.txt'
    check1=os.path.isfile(stockPath1)

    stockPath2 = '../data/stockfiles/' + stock2 + '.txt'
    check2=os.path.isfile(stockPath2)

    if check1 and check2:
        stockFile1 = np.loadtxt(stockPath1, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        stockFile2 = np.loadtxt(stockPath2, delimiter=',', skiprows=1, usecols=(4,), unpack=False)

        if min(len(stockFile1),len(stockFile2)) > entry+period:

            #a = np.cov(stockFile1[entry:entry+period],stockFile2[entry:entry+period])[0][1]
            #b = np.std(stockFile1[entry:entry+period])
            #c = np.std(stockFile2[entry:entry+period])

            stock1Squared = []
            stock2Squared = []
            stock1TimesStock2 = []

            for i in range(period):
                stock1Squared.append(stockFile1[entry+i]*stockFile1[entry+i])
                stock2Squared.append(stockFile2[entry+i]*stockFile2[entry+i])
                stock1TimesStock2.append(stockFile1[entry+i]*stockFile2[entry+i])

            a = np.mean(stock1TimesStock2) - np.mean(stockFile1[entry:entry+period])*np.mean(stockFile2[entry:entry+period])
            b = np.mean(stock1Squared) - np.mean(stockFile1[entry:entry+period])**2
            c = np.mean(stock2Squared) - np.mean(stockFile2[entry:entry+period])**2

            corr = a/((b*c)**0.5)
            
    return corr







        
        


