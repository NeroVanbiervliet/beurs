import matplotlib.pyplot as plt
from ADLDefinitions import ADLCalculator
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
import random

stockList = loadstocklist(0)
stock = stockList[random.randint(0,len(stockList)-1)]

ADL = ADLCalculator(stock,0,250)
stockPath = '../data/stockfiles/' + stock + '.txt'
stockFile = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)

plt.subplot(2,1,1)
plt.plot(stockFile[:250])

plt.subplot(2,1,2)
plt.plot(ADL)

plt.show()
            

