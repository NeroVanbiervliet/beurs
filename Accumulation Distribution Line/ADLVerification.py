import sys
sys.path.insert(0, '../')
from ADLDefinitions import ADLCalculator
from CommonDefinitions import *
import random
import matplotlib.pyplot as plt
import scipy.stats as st
import numpy as np


def ADLVerification(period,maxRicoDiff,samples):

    stockList = loadstocklist(0)

    shortPeriod = period
    midPeriod = period*2
    longPeriod = period*3

    avgPeriod = period

    allRicoDiff = []
    allResults = [[],[],[]]

    count = 0
    count2 = 0

    while count < samples and count2 < 2000:
        count2 += 1  

        if count%10 == 0:
            print count

        stock = stockList[random.randint(0,len(stockList)-1)]
        
        entry = random.randint(100,1000)

        ADL = ADLCalculator(stock,entry,250)

        if len(ADL) > avgPeriod:
            
            stockPath = '../data/stockfiles/' + stock + '.txt'
            stockFile = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
            
            ADLPercentage = []
            for i in range(avgPeriod):
                ADLPercentage.append((ADL[i] - ADL[i+1])/ADL[i+1])
                
            stockPercentage = []
            for i in range(avgPeriod):
                stockPercentage.append((stockFile[entry+i] - stockFile[entry+i+1])/stockFile[entry+i+1])

            ADLRico = np.mean(ADLPercentage)
            #print ADLRico
            stockRico = np.mean(stockPercentage)
            #print stockRico

            ricoDiff = ADLRico - stockRico

            if abs(ricoDiff) > maxRicoDiff and ((ADLRico > 0 and stockRico < 0) or (ADLRico < 0 and stockRico > 0)):
                count += 1
                resultShort = (stockFile[entry-shortPeriod] - stockFile[entry])/stockFile[entry]
                resultMid = (stockFile[entry-midPeriod] - stockFile[entry])/stockFile[entry]
                resultLong = (stockFile[entry-longPeriod] - stockFile[entry])/stockFile[entry]
                
                allRicoDiff.append(ricoDiff)
                allResults[0].append(resultShort)
                allResults[1].append(resultMid)
                allResults[2].append(resultLong)

    [m1,q1] = np.polyfit(allRicoDiff,allResults[0],1)
    [m2,q2] = np.polyfit(allRicoDiff,allResults[1],1)
    [m3,q3] = np.polyfit(allRicoDiff,allResults[2],1)

    plt.subplot(3,1,1)
    [m,q] = np.polyfit(allRicoDiff,allResults[0],1)
    values = []
    x = []
    for i in range(41):
        values.append(m*(i*0.01-0.2)+q)
        x.append(i*0.01-0.2)
    plt.plot(x,values)
    plt.scatter(allRicoDiff,allResults[0])
    plt.axis([-0.3,0.3,-0.3,0.3])
    plt.xlabel('RicoDiff')
    plt.ylabel('Percentage winst')
    plt.title('Samples:' + str(len(allRicoDiff)) + ' Short')
    plt.axhline(y=0)
    plt.axvline(x=0)
    plt.grid()

    plt.subplot(3,1,2)
    [m,q] = np.polyfit(allRicoDiff,allResults[1],1)
    values = []
    x = []
    for i in range(41):
        values.append(m*(i*0.01-0.2)+q)
        x.append(i*0.01-0.2)
    plt.plot(x,values)
    plt.scatter(allRicoDiff,allResults[1])
    plt.axis([-0.3,0.3,-0.3,0.3])
    plt.xlabel('RicoDiff')
    plt.ylabel('Percentage winst')
    plt.title('Samples:' + str(len(allRicoDiff)) + ' Mid')
    plt.axhline(y=0)
    plt.axvline(x=0)
    plt.grid()

    plt.subplot(3,1,3)
    [m,q] = np.polyfit(allRicoDiff,allResults[2],1)
    values = []
    x = []
    for i in range(41):
        values.append(m*(i*0.01-0.2)+q)
        x.append(i*0.01-0.2)
    plt.plot(x,values)
    plt.scatter(allRicoDiff,allResults[2])
    plt.axis([-0.3,0.3,-0.3,0.3])
    plt.xlabel('RicoDiff')
    plt.ylabel('Percentage winst')
    plt.title('Samples:' + str(len(allRicoDiff)) + ' Long')
    plt.axhline(y=0)
    plt.axvline(x=0)
    plt.grid()

    plt.show()

    return [m1,m2,m3]

[a,b,c]=ADLVerification(50,0.02,100)

##results = []
##
##for period in [20,40]:
##    print period
##    for maxRicoDiff in [0,0.01,0.03,0.05]:
##        print maxRicoDiff
##        [m1,m2,m3] = ADLVerification(period,maxRicoDiff)
##        results.append([period,maxRicoDiff,m1,m2,m3])
##
##maxm = -999
##bestPeriod = 0
##bestMaxRicoDiff = 0
##lala = 0
##for i in results:
##    if i[2] > maxm:
##        maxm = i[2]
##        bestPeriod = i[0]
##        bestMaxRicoDiff = i[1]
##        lala=1
##    if i[3] > maxm:
##        maxm = i[3]
##        bestPeriod = i[0]
##        bestMaxRicoDiff = i[1]
##        lala=2
##    if i[4] > maxm:
##        maxm = i[4]
##        bestPeriod = i[0]
##        bestMaxRicoDiff = i[1]
##        lala=3
##
##print bestPeriod
##print bestMaxRicoDiff
##print lala

    


