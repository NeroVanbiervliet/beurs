# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
from random import randint
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *



def ADLCalculator(stock,entry,length):

    ADL = []
    previousADL = 0

    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        stockLow = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(3,), unpack=False)
        stockHigh = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(2,), unpack=False)
        stockVolume = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(5,), unpack=False)

        for j in range(min(len(stockClose)-entry,length)):
            
            i = min(len(stockClose)-entry,length) + entry -1 -j
            
            if stockHigh[i] - stockLow[i] == 0:
                ADLvalue = previousADL
                
            else:
                MFM = ((stockClose[i] - stockLow[i]) - (stockHigh[i] - stockClose[i]))/(stockHigh[i] - stockLow[i])
                MFV = MFM * stockVolume[i]
                ADLvalue = previousADL + MFV
                
            previousADL = ADLvalue
            ADL.append(ADLvalue)

    return ADL[::-1]

        
    
