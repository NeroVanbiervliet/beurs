import matplotlib.pyplot as plt
from BBDefinitions import *
import numpy as np

timeRange = 500

plt.close("all")

stock = 'Ageas'
entry = 0

plt.plot(BollingerBandScore(stock,entry,99999)[:timeRange])

plt.show()
