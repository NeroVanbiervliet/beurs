# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
from random import randint
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *

def BollingerBandValue(stock,length):
    #length = 50
    period = 20
    middleBand = []
    
    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)

    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)

        for i in range(min(len(stockClose)-period,length)):
            middleBand.append(np.mean(stockClose[i:][:period]))

    return middleBand

def BollingerBandScore(stock,entry,length):

    scoreList = []
    diffList = []
    diffprev = 0
    count = 0
    score = 0
    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)

    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        BollingerBand = BollingerBandValue(stock,length)

        for i in range(len(BollingerBand)):
            diff = (stockClose[i] - BollingerBand[i])/stockClose[i]
            diffList.append(diff)

        for diff in diffList[::-1]:

            if count == 0:
                score = score*0.75
        
            if count > 0:
                score += diff*100.
                count += 1

            if count==4:
                count = 0
            
            if diffprev*diff < 0:
                score = 0
                count = 1
                
            diffprev = diff
            scoreList.append(score) 

                
    return scoreList[::-1][entry:]
        
 



            
        
        
       
        
        

        
    
