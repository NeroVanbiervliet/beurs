# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
from random import randint
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
    

def ADICalculator(stock,entry,length):

    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        stockLow = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(3,), unpack=False)
        stockHigh = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(2,), unpack=False)
        stockVolume = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(5,), unpack=False)
        print np.mean(stockClose[:length])
        #step1, calculate TR, +DM and -DM

        trueRange = []
        plusDM = []
        minusDM = []
        
        for i in range(len(stockClose)-1):
            
            TR1 = stockHigh[i] - stockLow[i]
            TR2 = abs(stockHigh[i] - stockClose[i+1])
            TR3 = abs(stockLow[i] - stockClose[i+1])
            trueRange.append(max([TR1,TR2,TR3]))
            plusDM.append(max((stockHigh[i] - stockHigh[i+1]),0))
            minusDM.append(max((stockHigh[i+1] - stockHigh[i]),0))

        #step2, smooth using Wilder 14 day period
        trueRange14 = WilderSmooth(trueRange,14)
        plusDM14 = WilderSmooth(plusDM,14)
        minusDM14 = WilderSmooth(minusDM,14)

        #step3, Plus Directional Movement (PlusDI14) calculation
        plusDI14 = []
        for i in range(len(plusDM14)):
            plusDI14.append(plusDM14[i]/trueRange14[i])

        #step4, Minus Directional Movement (minusDI14) calculation
        minusDI14 = []
        for i in range(len(minusDM14)):
            minusDI14.append(minusDM14[i]/trueRange14[i])

        DX = []
        #step5, Directional Movement Index (DX) calculation
        for i in range(len(plusDI14)):
            DX.append(abs(plusDI14[i]-minusDI14[i])/(plusDI14[i]+minusDI14[i])*100.)


        #step6, Average Directional Index (ADX) calculation
        start = np.mean(DX[-14:])
        ADX = [start]
        avgDXPrev = start
        for i in range(len(DX)-14):
            value = (avgDXPrev*13. + DX[-(14+i+1)])/14
            ADX.append(value)
            avgDXPrev = value
            
        ADX = ADX[::-1]
    
    return ADX


def WilderSmooth(list,period):
    
    start = sum(list[-period:])
    smoothed = [start]
    for i in range(len(list)-period-1):
        value = smoothed[i] - smoothed[i]/float(period) + list[-(i+1)]
        smoothed.append(value)
        
    return smoothed[::-1]
        
        
        

        
    
