import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
import matplotlib.pyplot as plt
import numpy as np
import random
import pickle
import os

def calcBreakPoints(stockfile,viewWindow):


    breakPointLength = int(4 + viewWindow/100)
    breakPointSpace = int(3 + viewWindow/100)
    fitGap = 0
    maxErrorAngle = 60./180.*3.1415

    
    breakPointList = []

    ratio = viewWindow/(1.3-0.7)
    
    entry = breakPointLength-1+fitGap

    stockfileS = stockfile_smoother(stockfile)
    
    
    while entry < len(stockfileS) - (breakPointLength+fitGap)-1:

        x1 = range(breakPointLength)
        x2 = range(breakPointLength)

        y1 = np.multiply(stockfileS[entry+1-fitGap-breakPointLength:entry+1-fitGap],ratio)
        y2 = np.multiply(stockfileS[entry+fitGap:entry+fitGap+breakPointLength],ratio)
        

        rico1 = np.polyfit(x1,y1,1)[0]
        rico2 = np.polyfit(x2,y2,1)[0]
        angle1 = np.arctan(rico1)
        angle2 = np.arctan(rico2)

        if abs(angle1-angle2) > maxErrorAngle:
            #print rico1,rico2,angle1/3.1415*180.,angle2/3.1415*180.

            if angle1 >= 0 and angle2 <= 0:
                # op zoek naar maximum in die range, dat is het breakpoint
                dummy = max(stockfile[entry:entry+breakPointLength])
                for i in range(breakPointLength):
                    if abs(dummy - stockfile[entry+i]) < 0.0000000000001:
                        ifinal = i

                breakPointList.append(entry+ifinal)
                        
                entry += breakPointSpace +i
                        
            if angle1 <= 0 and angle2 >= 0:
                # op zoek naar minimum in die range, dat is het breakpoint
                dummy = min(stockfile[entry:entry+breakPointLength])
                for i in range(breakPointLength):
                    if abs(dummy - stockfile[entry+i]) < 0.0000000000001:
                        ifinal = i
                        
                breakPointList.append(entry+ifinal)
                       
                entry += breakPointSpace +i

            if angle1 >= 0 and angle2 >= 0:
                #nog niks voor gevonden,komt amper voor
                entry += 1

            if angle1 <= 0 and angle2 <= 0:
                #nog niks voor gevonden,komt amper voor
                entry += 1
            
        else:
            entry += 1
            
    return breakPointList

viewWindow = 120

stockList = loadstocklist(0)
stock = stockList[random.randint(0,len(stockList)-1)]
#for stock in stockList:


stockpath = '../data/stockfiles/' + stock + '.txt'
check = os.path.isfile(stockpath)
if check:
    stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
    stockfile = stockfile_normaliser(stockfile1,min(1500,len(stockfile1)-1))
    stockfileS = stockfile_smoother(stockfile[0])

##        directory = '../data/breakpoint/' + stock
##        
##        if not os.path.exists(directory):
##            os.makedirs(directory)
##        
##        breakPointList = calcBreakPoints(stockfile[0],120)
##        path =  directory + '/Short.txt'
##        f = open(path,'w')
##        pickle.dump(breakPointList,f)
##        f.close()

    breakPointList = calcBreakPoints(stockfile[0],viewWindow)
        
    breakPointValues = []                    
    for breakPoint in breakPointList:
        breakPointValues.append(stockfile[0][breakPoint])
    
        
    plt.scatter(breakPointList,breakPointValues)    
    plt.plot(stockfile[0])
    plt.plot(stockfileS)
    plt.axis([0,viewWindow,0.5,1.5])
    plt.show()
