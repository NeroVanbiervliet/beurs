import matplotlib.pyplot as plt
from A_definitions import *

def calcBreakPoints(stockFile):
    
    classification = 0.25 
    downUpTolerance = 0.25
    baseIntervalLength = 4
    faultsInRowToQuit = 6    
    
    breakPoints = [] 
    
    if len(stockFile) < 20 :
        print 'stock file contains not enough data (<20)'
    else:
        lastBreakPointIndex = 0
        while lastBreakPointIndex +1 < len(stockFile):           
            
            intervalLength = baseIntervalLength            
            intervalRico = (stockFile[lastBreakPointIndex + intervalLength -1] - stockFile[lastBreakPointIndex])/intervalLength
           
            if intervalRico > classification: # stijgende boel
                increasing = True
                numDaysFaultIncrease = 0 
                while increasing:
                    intervalLength += 1
                    intervalRicoNew = (stockFile[lastBreakPointIndex + intervalLength -1] - stockFile[lastBreakPointIndex])/intervalLength
                    
                    if intervalRicoNew < intervalRico*(1-downUpTolerance) or intervalRicoNew > intervalRico*(1+downUpTolerance): 
                        numDaysFaultIncrease += 1
                    else: 
                        numDaysFaultIncrease = 0
                        
                    #intervalRico = intervalRicoNew
                    
                    if lastBreakPointIndex + intervalLength +1 > len(stockFile):
                        increasing = False
                    
                    if numDaysFaultIncrease == faultsInRowToQuit:
                        increasing = False
                        intervalLength -= numDaysFaultIncrease
            
            elif intervalRico < -classification: # dalende boel
                increasing = True
                numDaysFaultIncrease = 0 
                while increasing:
                    intervalLength += 1
                    intervalRicoNew = (stockFile[lastBreakPointIndex + intervalLength -1] - stockFile[lastBreakPointIndex])/intervalLength
                    
                    if intervalRicoNew < intervalRico*(1-downUpTolerance) or intervalRicoNew > intervalRico*(1+downUpTolerance): 
                        numDaysFaultIncrease += 1
                    else: 
                        numDaysFaultIncrease = 0
                        
                    #intervalRico = intervalRicoNew                   
                    
                    if lastBreakPointIndex + intervalLength +1 > len(stockFile):
                        increasing = False                    
                    
                    if numDaysFaultIncrease == faultsInRowToQuit:
                        increasing = False
                        intervalLength -= numDaysFaultIncrease
            
            else: # platte boel
                print 'platte'
                increasing = True
                while increasing:
                    intervalLength += 1
                    intervalRico = (stockFile[lastBreakPointIndex + intervalLength -1] - stockFile[lastBreakPointIndex])/intervalLength

                    if lastBreakPointIndex + intervalLength +1 > len(stockFile):
                        increasing = False   
                    
                    
                    if intervalRico > classification or intervalRico < -classification:
                        increasing = False
              
              
            breakPoints.append(lastBreakPointIndex+intervalLength)
            lastBreakPointIndex = lastBreakPointIndex+intervalLength
            plt.axvline(x=lastBreakPointIndex)
            print lastBreakPointIndex
            
    plt.plot(stockFile)
    plt.show()
    
    print 'stockFile done' 
    
    return breakPoints
    
# START


bel20 = loadstocklist(1)

stock = bel20[3]
stockpath = 'data/stockfiles/' + stock + '.txt'
check = os.path.isfile(stockpath)

if check:
    stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
    stockfile = stockfile_normaliser(stockfile1)
    stockfile = np.multiply(stockfile,1000)

breakPoints = calcBreakPoints(stockfile[:100])
    