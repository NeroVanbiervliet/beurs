import matplotlib.pyplot as plt
from A_definitions import *
import numpy as np
import random
def calcBreakPoints(stockFile,minBreakPointSpace,acceptanceGainDiff):  

    
    breakPoints = [] 
    
    if len(stockFile) < 20 :
        print 'stock file contains not enough data (<20)'
    else:
        percentageList = []        
        for i in range(len(stockFile)-1):
            percentageList.append((stockFile[i+1]/stockFile[i] - 1))
                                     
        startReference = -1
        
        #while startReference + minBreakPointSpace*2 - 1 < len(stockFile):
        while startReference + minBreakPointSpace*2 - 1 < len(percentageList)-1: 
            startReference += 1
            avg1 = np.mean(percentageList[startReference:startReference + minBreakPointSpace])
            avg2 = np.mean(percentageList[startReference+ minBreakPointSpace-1:startReference + minBreakPointSpace*2-1])
            
            if abs(avg1-avg2) > acceptanceGainDiff:
                BreakPoint = startReference + minBreakPointSpace-1
                bestDiff = abs(avg1-avg2)
                bestEntry = 0
                entry = 0
                searching = True
                lol = 0
                while entry > minBreakPointSpace and searching == True:
                    entry +=1
                    
                    avgTemp1 = np.mean(percentageList[startReference+entry:startReference+entry + minBreakPointSpace])
                    avgTemp2 = np.mean(percentageList[startReference+entry+ minBreakPointSpace-1:startReference+entry + minBreakPointSpace*2-1])
                    
                    if bestDiff < abs(avgTemp1-avgTemp2):
                        bestDiff = abs(avgTemp1-avgTemp2)
                        BreakPoint = startReference + minBreakPointSpace + entry-1
                        bestEntry = entry
                        lol = 0
                    else:
                        lol +=1
                    if lol > 2:
                        searching = False
                breakPoints.append(BreakPoint)
                startReference += minBreakPointSpace-1 + bestEntry
            
        
            
                
#            startReference += 1
#            referenceX = range(minBreakPointSpace)
#            referenceY = stockFile[startReference:startReference+minBreakPointSpace]
#
#            referenceRico = polyfit(referenceX,referenceY,1)[0]
#            
#            newX = range(minBreakPointSpace)
#            newY = stockFile[startReference+minBreakPointSpace-1:startReference+minBreakPointSpace*2-1]
#
#            newRico = polyfit(newX,newY,1)[0]
#            
#            coneBoundaryHigh = np.tan(np.arctan(referenceRico) + acceptanceConeRadians)
#            coneBoundaryLow = np.tan(np.arctan(referenceRico) - acceptanceConeRadians)
#            
#            if (newRico - coneBoundaryHigh) > 0 or (newRico - coneBoundaryLow) < 0: 
#                # niet in de cone
#                BreakPoint = startReference + minBreakPointSpace
#                breakPoints.append(BreakPoint)
#                startReference += minBreakPointSpace-1
                
    breakPointValues = []                    
    for breakPoint in breakPoints:
        breakPointValues.append(stockFile[breakPoint])
        #plt.axvline(x=breakPoint)
    plt.scatter(breakPoints,breakPointValues)
    plt.plot(stockFile)
    title = 'BreakPointSpace = ' + str(minBreakPointSpace) + ' acceptanceGainDiff = ' + str(acceptanceGainDiff) 
    plt.title(title)
    plt.savefig('tests/' + str(minBreakPointSpace) + '_' + str(int(acceptanceGainDiff*1000)) + str(count))
    plt.close()
    print 'stockFile done' 
    
    return breakPoints
    
# START


bel20 = loadstocklist(1)

stock = bel20[3]
stockpath = 'data/stockfiles/' + stock + '.txt'
check = os.path.isfile(stockpath)
if check:
    stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
    stockfile = stockfile_normaliser(stockfile1)
    #stockfile = np.multiply(stockfile,1000)
    
global count
count = 0

for minBreakPointSpace in [5]:
    for acceptanceGainDiff in [7,9]:
        acceptanceGainDiff = acceptanceGainDiff*0.001
        
        for i in range(5):
            a = random.randint(0,len(bel20)-1)
            stock = bel20[a]
            stockpath = 'data/stockfiles/' + stock + '.txt'
            check = os.path.isfile(stockpath)
            count += 1
            if check:
                stockfile1 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                stockfile = stockfile_normaliser(stockfile1)
                b = random.randint(0,len(stockfile)-100)
        
                breakPoints = calcBreakPoints(stockfile[b:b+100],minBreakPointSpace,acceptanceGainDiff)



    