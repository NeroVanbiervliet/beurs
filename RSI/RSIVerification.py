import sys
sys.path.insert(0, '../')
from RSIDefinitions import RSICalculator
from CommonDefinitions import *
import random
import matplotlib.pyplot as plt
import scipy.stats as st

stockList = loadstocklist(0)

period = 30

avgPeriod = 6

results1 = []
results2 = []

allRSI1 = []
allRSI2 = []

count = 0
RSIList = []

while count < 100:
    

    if count%10 == 0:
        print count

    stock = stockList[random.randint(0,len(stockList)-1)]
    
    entry = random.randint(100,1000)

    RSI = RSICalculator(stock,entry)

    if RSI:
        stockPath = '../data/stockfiles/' + stock + '.txt'
        stockFile = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        average = np.mean(stockFile[entry:entry+200])

        RSI = np.mean(RSI[:avgPeriod])

        if RSI<35 or RSI>65:
            count += 1        
            result = (np.mean(stockFile[entry-period-1:entry-period+1])-stockFile[entry])/stockFile[entry]
            
            if stockFile[entry] > average:
                allRSI1.append(RSI)
                results1.append(result)
            else:
                allRSI2.append(RSI)
                results2.append(result)
        
    
plt.subplot(2,1,1)
plt.scatter(allRSI1,results1)
plt.axis([0,100,-0.30,0.30])
plt.xlabel('RSI')
plt.ylabel('Percentage winst')
plt.title('Samples:' + str(len(allRSI1)) + ' above average')
plt.grid()

plt.subplot(2,1,2)
plt.scatter(allRSI2,results2)
plt.axis([0,100,-0.30,0.30])
plt.xlabel('RSI')
plt.ylabel('Percentage winst')
plt.title('Samples:' + str(len(allRSI2)) + ' below average')
plt.grid()

plt.show()


