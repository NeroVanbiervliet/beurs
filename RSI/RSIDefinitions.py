# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
from random import randint
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *

def RSIScore(stock,entry):
    score = 0
    redFactor = 0.6

    RSIprev = 0

    scoreList = []
    
    RSI = RSICalculator(stock,entry)
    for i in RSI[::-1]:
        
        if i<0 or i>100:
            print 'i not in interval [0,100]'        
        
        if i < 30:
            score += min(-2./10.*i + 7.,3)
        if i > 70:
            score += max(-2./10.*i + 13.,-3)
        if i < 70 and i > 30:
            score = score*redFactor

        if (RSIprev-50)*(i-50) < 0:
            score = 0

        scoreList.append(score)
        RSIprev = i

    return scoreList[::-1]
          


def RSICalculator(stock,entry):
    period = 14
    #length = 250
    gainList = []
    lossList = []
    RSI = []
    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)

    
    if not check:
        print stock + 'not available'
    else:
        stockFile1 = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                    
        for i in range(len(stockFile1)-1):
 
            diff = (stockFile1[::-1][i+1] - stockFile1[::-1][i])/max(stockFile1[::-1][i],0.01)
            
            if i == 14:
                avgGain = sum(gainList)/max(len(gainList),1) 
                avgLoss= sum(lossList)/max(len(lossList),1)
                
            if i < period:
            
                if diff > 0:
                    gainList.append(diff)
                else:
                    lossList.append(abs(diff))
                
            else:
                avgGain = (avgGain*13. + max(diff,0))/14.
                avgLoss = (avgLoss*13. + abs(min(diff,0)))/14.
                if avgGain == 0:
                    RSI.append(0)
                else:
                    if avgLoss == 0:
                        RSI.append(100)
                    else:
                        RS = avgGain/avgLoss
                        RSIvalue = 100.-100./(1.+RS)
                        RSI.append(RSIvalue)

    return RSI[::-1][entry:]

        
