import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
from RSIDefinitions import RSIScore
import matplotlib.pyplot as plt
stockList = loadstocklist(0)
import random
import numpy as np
import scipy.stats

plt.close('all')
# 'HSIC'
minValue = 0.0
maxValue = 0.0
i = 100 # 43 is Sipef
minList = []
maxList = []
resultOfAll = []

while minValue>-10000 and maxValue<10000 and i<(len(stockList)-2):    
    print i
    i= i+1
    stock = stockList[i]
    result = RSIScore(stock,1500)
    resultPart = result[950:995]
    if len(result) != 0:
        minValue = min(result)
        maxValue = max(result)
        minList.append(minValue)
        maxList.append(maxValue)

    resultOfAll.extend(result)

roundedResult = [ round(elem, 2) for elem in resultOfAll ]

#nullen er uit filteren
filteredResult = []
for number in roundedResult:
    if number != 0 :
        filteredResult.append(number)

plt.hist(filteredResult,100)
axes = plt.gca()
axes.set_xlim([-50,50])
