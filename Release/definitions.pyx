# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import datetime
import pickle
import time

cdef int errorCalculator(list stockfile,list theory,int end,int sumdays):
    cdef int i
    cdef int j
    cdef int daycount
    cdef int running
    cdef float error,theoryPiece1,theoryPiece2,theoryPiece3
	
    daycount = 0
    running = 1
    # eerste iteratie van i apart    
    i=0
    error = 0
    j=0 
    while j < theory[4*i+3] and running==1:       
        error = error + abs(stockfile[end + sumdays-1 -  j - daycount] - (theory[0]/1000. + theory[1]/1000.*(j) + theory[2]/1000.*(j)*(j))) 
        if error/theory[4*i+3] > 0.01:
            running = 0
        j += 1     
    daycount += theory[3]
    i += 1
    
    # alle volgende iteraties van i     
    while i < int(len(theory)/4) and running==1:    
        error = 0.     
        theoryPiece1 = theory[4*i]/1000.        
        theoryPiece2 = theory[4*i+1]/1000.        
        theoryPiece3 = theory[4*i+2]/1000.              
        j=0
        while j < theory[4*i+3] and running==1:       
            error = error + abs(stockfile[end + sumdays-1 -  j - daycount] - (theoryPiece1 + theoryPiece2*(j+1) + theoryPiece3*(j+1)*(j+1)))
            if error/theory[4*i+3] > 0.01:
                running = 0			
            j += 1
        daycount += theory[4*i+3]
        i += 1
        
    return running

cdef theoryGenerator():
    cdef int amount, start,rise1,rise2,days1,start2,rise3,rise4,days2,start3,rise5,rise6,days3,start4
    
    theory_list = []

    amount=0

    
    for start in [1000,1020,980,1040,960,1060,940,1080,920]:
          
        for rise1 in [0,10,-10,20,-20,30,-30]:
                
            for rise2 in [0,5,-5]:
                 
                for days1 in [5,7]:
                    
                    start2 = start + (days1-1)*rise1+ (days1-1)*(days1-1)*rise2

                    if abs(start2 - 1000) < 400:  
                    
                        for rise3 in [0,10,-10,20,-20,30,-30]:
                            
                            for rise4 in [0,5,-5]:
                                 
                                for days2 in [5,7]:
                                    start3 = start2 + (days2-1)*rise3+ (days2-1)*(days2-1)*rise4

                                    if abs(start3 - start2) < 400:

                                        for rise5 in [0,10,-10,20,-20,30,-30]:

                                            for rise6 in [0,5,-5]:

                                                for days3 in [5,7]:
                                                    start4 = start3 + (days3-1)*rise5+ (days3-1)*(days3-1)*rise6

                                                    if abs(start4 - start3) < 400:

                                                        theory_list.append([start,rise1,rise2,days1,start2,rise3,rise4,days2,start3,rise5,rise6,days3])            

                                        theory_list.append([start,rise1,rise2,days1,start2,rise3,rise4,days2])
##                                        start1_list.append(start)
##                                        rise1_list.append(rise1)
##                                        rise2_list.append(rise2)
##                                        days1_list.append(days1)
##                                        start2_list.append(start2)
##                                        rise3_list.append(rise3)
##                                        rise4_list.append(rise4)
##                                        days2_list.append(days2)

                                        amount += 1
                                
    #print '#of theories:', amount
    return theory_list

def stockfile_normaliser(stockfile):

    cdef int i,b
    cdef float avg
    cdef list stockfile_corrected
    stockfile_corrected = []

    b = 120

    avg = sum(stockfile[:b])/b
    
    for i in xrange(len(stockfile)):

        stockfile_corrected.append(stockfile[i]/avg)

        if i+b+1 < len(stockfile):

            avg = avg - stockfile[i]/b + stockfile[i+b]/b

            # moving average

        
    return stockfile_corrected

cdef theory_fit(list stockfile,list theorylist):
    cdef int i,j,sumdays
    
    theory_fit_list = []
    
    for i in range(len(theorylist)): #alle theorien afgaan
        sumdays = 0
        for j in range(int(len(theorylist[i])/4)):
            sumdays = sumdays + theorylist[i][4*j+3]

        if errorCalculator(stockfile,theorylist[i],0,sumdays):
               
               theory_fit_list.append(theorylist[i])
                          
    return theory_fit_list


def loadstocklist(n):
    
    stocklist=np.loadtxt('data/stockfiles/stocklist.txt',dtype='string', delimiter=',', skiprows=0, usecols=(0,), unpack=False)

    if n==1:
        #BEL20
        stocklist=stocklist[:50]

    if n==2:
        #NASDAQ
        stocklist=stocklist[50:150]
        
    if n==3:
        #Dow jones
        stocklist=stocklist[150:179]
        
    if n==4:
        #FTSE
        stocklist=stocklist[179:203]
        
    if n==5:
        #AEX
        stocklist=stocklist[203:227]
        
    #['AGB.BR','AVH.BR','BEFB.BR','BEK.BR','BLG.BR','COFB.BR','COL.BR','DEL.BR','DIE.BR','DL.BR','ELI.BR','GBL.BR','GSZ.BR','INT.BR','KBC.BR','SOL.BR','THR.BR','TNO.BR','UCB.BR','UMC.BR']

    return stocklist

    ## Deze definitie gaat een lijst met aandelen laden, gewoon de naam ervan. Als de input 1 is, dan doet hij enkel de BEL20

def resultAnalyser(stockfile,end,days_after):

    results_array = []
    
    for i in range(int(days_after)):
        results_array.append(max(min((stockfile[end-1-i] - stockfile[end])/(stockfile[end]),0.5),-0.5))
        
    return results_array  

cdef historic(list stockfile,list theory):
    cdef int sumdays,i,end,end2
    cdef list resultslist
    sumdays = 0
    resultslist = []
    
    for i in range(int(len(theory)/4)):
        sumdays = sumdays + theory[4*i+3]


    for end in xrange(min(len(stockfile)-sumdays-20,1000)):
        end2 = end + 20
                    
        if errorCalculator(stockfile,theory,end2,sumdays):
            result = resultAnalyser(stockfile,end2,20)
            resultslist.append(result)
    
    return resultslist


def predictor(results):
    
    day1 = []
    day2 = []
    day3 = []
    day4 = []
    day5 = []
    day6 = []
    day7 = []
    day8 = []
    day9 = []
    day10 = []
    day11 = []
    day12 = []
    day13 = []
    day14 = []
    day15 = []
    day16 = []
    day17 = []
    day18 = []
    day19 = []
    day20 = []
    amount = 0
    
    for i in range(len(results)):
        for j in range(len(results[i])):
            amount += 1
            
            day1.append(results[i][j][0])
            day2.append(results[i][j][1])
            day3.append(results[i][j][2]) 
            day4.append(results[i][j][3])
            day5.append(results[i][j][4])
            day6.append(results[i][j][5])
            day7.append(results[i][j][6])
            day8.append(results[i][j][7])
            day9.append(results[i][j][8]) 
            day10.append(results[i][j][9]) 
            day11.append(results[i][j][10]) 
            day12.append(results[i][j][11]) 
            day13.append(results[i][j][12]) 
            day14.append(results[i][j][13]) 
            day15.append(results[i][j][14]) 
            day16.append(results[i][j][15]) 
            day17.append(results[i][j][16]) 
            day18.append(results[i][j][17]) 
            day19.append(results[i][j][18])
            day20.append(results[i][j][19])

    expectations = []
    dev = []
    probability = []
    for i in range(20):

        name = 'day' + str(i+1)
        expectations.append(np.mean(eval(name)))
        #dev.append(stdev(eval(name)))
        probability.append(1-st.norm.cdf(-np.mean(eval(name))/np.std(eval(name))))
        
    return expectations,probability,amount
                            
def method1(stockfile,stock):
    theorylist = theoryGenerator()
    theoryfit = theory_fit(stockfile,theorylist)
    expectation = []
    probability = []
    amount = 0
    results = []
    
    if theoryfit: #if list is empty, this will be skipped
        
        market = marketCheck(stock)
        
        stocklist = loadstocklist(market)
        
        for theory in theoryfit:

            print theory

            index = np.loadtxt('data/theorydata/theoryindex.txt',dtype='string', delimiter=';', skiprows=0, usecols=(0,1,2), unpack=False)
            
            load = False
            for i in range(len(index)):
                if index[i][1] == str(theory):
                    if index[i][2] == str(market):
                        load = True
                        entry = index[i][0]
            if load:
                path = 'data/theorydata/' + str(entry) + '.txt'
                f = open(path,'r')
                results = pickle.load(f)
                print 'loaded'
                f.close()
                

                        
            else:
                for stock in stocklist:
                    stockpath = 'data/stockfiles/' + stock + '.txt'
                    check=os.path.isfile(stockpath)
                    if check:
                        try:
                            stockfile2 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                            stockfile2 = stockfile_normaliser(stockfile2)
                            resultsTemp = historic(stockfile2,theory)
                            results.append(resultsTemp)
                            
                        except ValueError:
                            print stockpath + ' not available'
                path = 'data/theorydata/' + str(len(index)) + '.txt'
                f = open(path,'a+')
                pickle.dump(results,f)
                f.close()
                f = open('data/theorydata/theoryindex.txt','a+')
                dump = str(len(index)) + ';' + str(theory) + ';' + str(market)
                f.write(dump)
                f.write('\n')
                f.close()
            
            
        expectation, probability, amount = predictor(results)

              
    return expectation, probability, amount

def marketCheck(stock):
    market = 0
    stocklist = loadstocklist(0)
    for i in range(len(stocklist)):
        if stocklist[i] == stock:
            if i < 50:
                market = 1
                
            if i > 49 and i < 150:
                market = 2

            if i > 149 and i < 179:
                market = 3
                
            if i > 178 and i < 203:
                market = 4
                
            if i > 202 and i < 227:
                market = 5
                
    return market

def resultsCheck():
    directory = 'data/results/' + str(datetime.datetime.now().date())
    check = os.path.isdir(directory)
    
    return check

    
def makeGraphResults(E,stockfile,stock):
    Enew = []
    for i in range(len(E)):
        Enew.append(stockfile[0]*(1+E[i]))
    days1 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
    days2 = [31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50]
    plt.figure(figsize=(6.5, 5.5))
    plt.plot(days1,stockfile[::-1][-30:])
    plt.plot(days2[:len(E)],Enew)
    u = np.mean(stockfile[::-1][-30:])
    plt.ylim([0.9*u,1.1*u])
    plt.title(stock)
    plt.savefig('images/graph'+stock+'.png')

def resultLoaderCombined(market):
    resultsFinal = []
    date = str(datetime.datetime.now().date())
    month = date[5:7]
    day = date[8:10]
    datelist = ['2014-10-11','2014-10-10','2014-10-09','2014-10-08','2014-10-07','2014-10-04','2014-10-03','2014-10-02','2014-10-01','2014-09-30','2014-09-29','2014-09-28','2014-09-27','2014-09-26','2014-09-25','2014-09-24','2014-09-23','2014-09-20','2014-09-19','2014-09-18','2014-09-17','2014-09-16','2014-09-13','2014-09-12','2014-09-11','2014-09-10','2014-09-09','2014-09-06','2014-09-05']
    i = 0
    count = 0
    while not i==20 and count < 20:
        datenew = datelist[count]
        count += 1   
        path = 'data/results/' + datenew + '/results' + str(market) + '.txt'
        check = os.path.isfile(path)
        if check:
            
            f = open(path,'r')
            result = pickle.load(f)
            f.close()
            for j in range(len(result)):
                stock = result[j][0]
                found = False
                for entry in range(len(resultsFinal)):
                    if resultsFinal[entry][0] == stock:
                        found = True
                        index = entry
                if found:
                    resultsFinal[index][1].append(result[j][1][:(20-i)])
                    resultsFinal[index][2].append(result[j][2][:(20-i)])
                    resultsFinal[index][3].append(result[j][3])
                    
                else:
                    resultsFinal.append([result[j][0],[result[j][1][:(20-i)]],[result[j][2][:(20-i)]],[result[j][3]]])
        
            i += 1
        
        
    if resultsFinal:
        for j in range(len(resultsFinal)): #alle stocks overlopen
            amountnew = 0
            if not len(resultsFinal[j][1]) == 1: #als er meerdere resultaten zijn
                
                Etemp = []
                Enew = []
                for i in range(len(resultsFinal[j][1])): #alle resultaten overlopen
                    for p in range(len(resultsFinal[j][1][i])):
                        if i == 0:
                            Etemp.append([])
                        Etemp[p].append(resultsFinal[j][1][i][p])
                        
                for i in range(len(Etemp)):
                    Enew.append(np.mean(Etemp[i]))
                
                resultsFinal[j][1] = Enew

                Ptemp = []
                Pnew = []
                for i in range(len(resultsFinal[j][2])):
                    for p in range(len(resultsFinal[j][2][i])):
                        if i == 0:
                            Ptemp.append([])
                        Ptemp[p].append(resultsFinal[j][2][i][p])
                        
                for i in range(len(Ptemp)):
                    Pnew.append(np.mean(Ptemp[i]))
                
                resultsFinal[j][2] = Pnew
                
                for i in range(len(resultsFinal[j][3])):
                    amountnew += resultsFinal[j][3][i]
                    
                amount = amountnew/len(resultsFinal[j][3])
                resultsFinal[j][3]= amount
                
            else:
                resultsFinal[j][1] = resultsFinal[j][1][0]
                resultsFinal[j][2] = resultsFinal[j][2][0]
                resultsFinal[j][3] = resultsFinal[j][3][0]
            
            
    return resultsFinal   
        
def find(value,list):
    found = False
    for i in range(len(list)):
        if value == list[i]:
            found = True
            
    return found

def method1special(stockfile,stock,entry):
    t = time.time()
    theorylist = theoryGenerator()
    theoryfit = theory_fit(stockfile,theorylist)
    expectation = []
    probability = []
    amount = 0
    results = []
    
    if theoryfit: #if list is empty, this will be skipped
        print 'amount of theories:', len(theoryfit)
        market = marketCheck(stock)
        
        stocklist = loadstocklist(market)
        
        for theory in theoryfit:
            
            for stock in stocklist:
                
                stockpath = 'data/stockfiles/' + stock + '.txt'
                check=os.path.isfile(stockpath)
                if check:
                    try:
                        stockfile2 = np.loadtxt(stockpath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                        stockfile2 = stockfile_normaliser(stockfile2)
                        if entry+30 < len(stockfile2):
                            resultsTemp = historic(stockfile2[entry:],theory)
                            results.append(resultsTemp)
                        
                    except ValueError:
                        print stockpath + ' not available'
                              
            
        expectation, probability, amount = predictor(results)
        amount = amount/len(theoryfit)
    print 'time:',(time.time()-t)          
    return expectation, probability, amount


def graphsaver(Packlist,title,name):
    plt.plot(Packlist)
    plt.title(title)
    plt.savefig('Simulations/' +name+ '.png')
    plt.close()

