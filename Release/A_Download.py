import urllib2
import csv
import sys
import os
import os.path
from A_definitions import loadstocklist
from time import gmtime, strftime
import numpy as np
import datetime
import pickle

stocklist=loadstocklist(0)
stocklist = stocklist[50:179]

#http://ichart.finance.yahoo.com/table.csv?s=YHOO

#stocklist=['AGB.BR','AVH.BR','BEFB.BR','BEK.BR','BLG.BR','COFB.BR','COL.BR','DEL.BR','DIE.BR','DL.BR','ELI.BR','GBL.BR','GSZ.BR','INT.BR','KBC.BR','SOL.BR','THR.BR','TNO.BR','UCB.BR','UMC.BR']
date = str(datetime.datetime.now().date())
f = open('lastdownloadupdate.txt','w')
pickle.dump(date,f)
f.close()

for i in range(len(stocklist)):
    label = str(stocklist[i])
    filename=label+'.txt'
    f=os.path.isfile('data/stockfiles/' + filename)

    date=strftime("%Y-%m-%d %H:%M:%S", gmtime())[:10]
    yesterday = str(datetime.date.fromordinal(datetime.date.today().toordinal()-1))
    
    stockdate='0'
    
    if f:
        stockdate=np.loadtxt('data/stockfiles/' + filename,dtype='string', delimiter=',', skiprows=1, usecols=(0,), unpack=False)
        stockdate=stockdate[0]

        if not yesterday==stockdate:
            print label
            os.remove('data/stockfiles/' + filename)
        

    f = os.path.isfile('data/stockfiles/' + filename)
    
    if f==False:
        
        url = str('http://ichart.finance.yahoo.com/table.csv?s='+ label)
        try:
            u = urllib2.urlopen(url)
            localFile = open('table.csv', 'w')
            localFile.write(u.read())
            localFile.close()
            
            os.rename('table.csv','data/stockfiles/' + filename)

        except urllib2.HTTPError:
            print filename, 'not found'

## BEL20 download en FTSE download
#http://www.euroinvestor.com/stock/historicalquotes.aspx?instrumentId=1798433&format=CSV
stocklistBEL20 = [['AB Inbev.csv', 1798433],
                  ['Ackermans V.Haaren.csv',344146],
                  ['Ageas.csv',2299754],
                  ['Befimmo-Sicafi.csv',344774],
                  ['Bekaert.csv',344687],
                  ['Belgacom.csv',440544],
                  ['Bpost.csv',15619391],
                  ['Cofinimmo-Sicafi.csv',344519],
                  ['Colruyt.csv',344689],
                  ['Delhaize Group.csv',344231],
                  ['Delta Lloyd.csv',2170765],
                  ['Dieteren.csv',344692],
                  ['Elia.csv',499453],
                  ['GBL.csv',344256],
                  ['GDF Suez.csv',1617881],
                  ['KBC.csv',344055],
                  ['Solvay.csv',344043],
                  ['Telenet Group.csv',511548],
                  ['UCB.csv',344206],
                  ['Umicore.csv',344289]]



stocklistFTSE = [['Anglo American PLC Ord USD0.54.csv',71313],
                 ['Astrazeneca PLC ORD SHS 0.25.csv',64723],
                 ['Barclays PLC.csv',313],
                 ['BG Group PLC ORD 10P.csv',378],
                 ['BHP Billiton PLC.csv',386],
                 ['BP PLC 0.25.csv',197278],
                 ['British American Tobacco PLC O.csv',501],
                 ['Diageo PLC.csv',933],
                 ['GlaxoSmithKline PLC.csv',245997],
                 ['Glencore PLC ORD USD0.01.csv',3605156],
                 ['Hsbc Hldgs.Uk ORD 0.50 UK RE.csv',1689],
                 ['Imperial Tobacco Group PLC.csv',1727],
                 ['Lloyds Banking Group PLC ORD 1.csv',2009],
                 ['National Grid PLC Ord 11 1743.csv',504404],
                 ['Reckitt BEN GP ORD 2000P.csv',115508],
                 ['RIO Tinto PLC ORD 10P.csv',2690],
                 ['Royal Dutch Shell PLC A Ord.csv',503824],
                 ['Royal Dutch Shell PLC B Ord.csv',504141],
                 ['Sabmiller PLC ORD 0.10.csv',30281],
                 ['Standard Chartered PLC ORD USD.csv',2964],
                 ['Tesco PLC.csv',3069],
                 ['Unilever PLC Ord 3 19p.csv',3230],
                 ['Vodafone Group PLC ORD USD0.20.csv',3297],
                 ['Xstrata PLC ORD USD0.50.csv',332743]]

BELMID = [['Ablynx.csv',674552],
          ['Aedifica.csv',593057],
          ['Agfa-Gevaert.csv',344175],
          ['Arseus.csv',663516],
          ['Barco.csv',344688],
          ['Bque NAT Belgique.csv',344887],
          ['Brederode.csv',345699],
          ['CFE.csv',354015],
          ['CIE Bois Sauvage.csv',346282],
          ['CMB.csv',344541],
          ['Deceuninck.csv',345156],
          ['Dieteren.csv',344692],
          ['Econocom Group.csv',346487],
          ['Elia.csv',499453],
          ['Euronav.csv',480618],
          ['EVS BROADC.EQUIPM..csv',346743],
          ['Exmar.csv',396364],
          ['Galapagos.csv',495514],
          ['Gimv.csv',345831],
          ['Intervest Off-Ware.csv',346451],
          ['Kinepolis Group.csv',344706],
          ['Melexis.csv',344604],
          ['Recticel.csv',346636],
          ['Retail EST.-SICAFI.csv',345333],
          ['RHJ International.csv',491995],
          ['RTL Group.csv',344967],
          ['Sipef.csv',345527],
          ['Sofina.csv',346130],
          ['Tessenderlo.csv',344691],
          ['Thrombogenics.csv',580079],
          ['VAN DE Velde.csv',344671],
          ['Wdp-Sicafi.csv',347281]]
          
AEX = [['Aegon.csv', 343962],
       ['Akzo Nobel.csv',344048],
       ['Arcelormittal.csv',482915],
       ['Asml Holding.csv',344070],
       ['Corio.csv',344657],
       ['Delta Lloyd.csv',2170765],
       ['DSM KON.csv',344042],
       ['Fugro.csv',344232],
       ['Gemalto.csv',575361],
       ['Heineken.csv',344084],
       ['ING Groep.csv',343951],
       ['Koninklijke Ahold NV Koninklij.csv',19769895],
       ['Koninklijke Boskalis Westminst.csv',2315041],
       ['KPN KON.csv',343990],
       ['OCI.csv',13660711],
       ['Philips KON.csv',343961],
       ['Randstad.csv',344377],
       ['Reed Elsevier.csv',344182],
       ['Royal Dutch Shella.csv',504159],
       ['SBM Offshore.csv',495213],
       ['TNT Express.csv',3630430],
       ['Unibail-Rodamco.csv',345037],
       ['Unilever DR.csv',344069],
       ['Wolters Kluwer.csv',344285],
       ['Ziggo.csv',8903362]]
        

        
stocklist = stocklistBEL20 + stocklistFTSE + BELMID + AEX

for i in range(len(stocklist)):

    ID = stocklist[i][1]
    filename = stocklist[i][0]

    f=os.path.isfile('data/stockfiles/' + filename[:-4] + '.txt')

    if f:
        stockdate=np.loadtxt('data/stockfiles/' + filename[:-4] + '.txt',dtype='string', delimiter=',', skiprows=1, usecols=(0,), unpack=False)
        stockdate=stockdate[0][:10]
        stockdate = stockdate[-4:] + '-' + stockdate[3:5] + '-' + stockdate[:2]
        if not yesterday==stockdate:
            
            os.remove('data/stockfiles/' + filename[:-4] + '.txt')

    
    f = os.path.isfile('data/stockfiles/' + filename[:-4] + '.txt')
    
    if not f:
        
        url = str('http://www.euroinvestor.com/stock/historicalquotes.aspx?instrumentId=' + str(ID) + '&format=CSV')
        try:
            u = urllib2.urlopen(url)
            localFile = open(filename, 'w')
            localFile.write(u.read())
            localFile.close()              
            os.rename(filename,'data/stockfiles/' + filename[:-4] + '.txt')
            print filename[:-4] 
            
        except urllib2.HTTPError:
                print filename, 'not found'
