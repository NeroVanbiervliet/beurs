from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'Definitions',
  ext_modules = cythonize("definitions.pyx"),
)
