import numpy as np
from numpy import loadtxt
import scipy.stats as st
import os.path
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
sys.path.insert(0, '../MACD')
from MACDDefinitions import *
sys.path.insert(0, '../BollingerBand')
from BBDefinitions import *
sys.path.insert(0, '../RSI')
from RSIDefinitions import *
sys.path.insert(0, '../Download')
from Download import Download

#Download() #momenteel nog 'dom' downloaden elke keer


#parameters voor beglische aandelen
BBScale = 5.9
MACDScale = 1.7
RSIScale = 1.1
totalThreshold = 30.

stockList = loadstocklist(1) #belgische aandelen laden
length = 300
for stock in stockList:

    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        #stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        BBScoreValue = BollingerBandScore(stock,0,300)[0]
        MACDScoreValue = MACDScore(stock,0,300)[0]
        RSIScoreValue = RSIScore(stock,0)[0]

        totalScore = BBScoreValue*BBScale + MACDScoreValue*MACDScale + RSIScoreValue*RSIScale

        if totalScore > totalThreshold:
            print 'buy ',stock

        




