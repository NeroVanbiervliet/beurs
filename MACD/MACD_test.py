import matplotlib.pyplot as plt
from MACDDefinitions import *
import numpy as np

timeRange = [0,500]

plt.close("all")

stock = 'Ageas'
entry = 100

fig = plt.figure()
figsub = fig.add_subplot(111)
fig.suptitle('MACDScore')
figsub.plot(MACDScore(stock,entry,99999))
figsub.set_xlim(timeRange)
figsub.set_ylim([-10,10])
figsub.axhline(y=0, color = 'green')

fig2 = plt.figure()
fig2sub = fig2.add_subplot(111)
fig2.suptitle('MACD')
fig2sub.plot(MACDCalculator(stock,entry,999999), color='orange', label='MACD')
fig2sub.plot(SignalLine(MACDCalculator(stock,entry,999999),99999),color='blue',label='signalLine')
fig2sub.plot(np.array(MACDScore(stock,entry,999999))/50, color='brown',label='scaledMACDScore')
fig2sub.set_xlim(timeRange)
fig2sub.set_ylim([-0.5,0.5])
fig2sub.legend(bbox_to_anchor=(0, 1), loc=2, borderaxespad=0.)

#==============================================================================
# fig3 = plt.figure()
# fig3sub = fig3.add_subplot(111)
# fig3.suptitle('MACDScorezoom')
# fig3sub.plot(MACDScore('ageas',0)[120:135], color='red',linestyle = '--',marker='o')
#==============================================================================

plt.show()
