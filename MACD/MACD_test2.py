import matplotlib.pyplot as plt
from MACDDefinitions import *

timeRange = [0,140]

plt.close("all")

    
# MACDScore code
# artificiele input voor MACDScore
MACD = [0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.1,0.1,0.1,0.1]
signalLine = [0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2,0.2]

scoreList = []
diffprev = 0
count = 0
difflist = []
score = 0

for i in range(len(signalLine)):
    diff = (MACD[i] - signalLine[i])/MACD[i] # relatief verschil
    difflist.append(diff)

for diff in difflist[::-1]: # beginnen met oudste diff dus

    if count == 0:
        score = score*0.5 # score laten afnemen
    
    if count>0:
        # nero score = diff*100. + 150*2.71**(0.5*(count-1))
        score = diff*10.
        count += 1

    if count==4: # na 4 dagen te ver van moment van wissel, dus reset
        count = 0
    
    if diffprev*diff < 0 and count ==0: # snijding MACD en signal line
        score = 3 # matthias 3
        count +=1
        
    diffprev = diff
    scoreList.append(score)

scoreList = scoreList[::-1]

plt.plot(scoreList)
plt.plot(MACD)
plt.plot(signalLine)
plt.show()
