# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
from random import randint
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *

def MACDScore(stock,entry,length):

    MACD = MACDCalculator(stock,entry,length)

    signalLine = SignalLine(MACD,length)

    scoreList = []
    diffprev = 0
    count = 0
    difflist = []
    score = 0
    
    for i in range(len(signalLine)):
        #diff = (MACD[i] - signalLine[i])/MACD[i] # relatief verschil, oud
        
        diff = (MACD[i] - signalLine[i])
        difflist.append(diff)

    for diff in difflist[::-1]: # beginnen met oudste diff dus

        if count == 0:
            score = score*0.8 # score laten afnemen
        
        if count>0:
            # nero score = diff*100. + 150*2.71**(0.5*(count-1))
            score = diff*500.
            count += 1

        if count == 4: # na 4 dagen te ver van moment van wissel, dus reset
            count = 0
        
        if diffprev*diff < 0: # snijding MACD en signal line
            score = 0 
            count +=1
            
        diffprev = diff
        scoreList.append(score)     
        
    return scoreList[::-1]

            

def MACDCalculator(stock,entry,length):

    period1 = 12
    period2 = 26
    #length = 200

    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        EMA1 = EMA(stockClose,period1,length)
        EMA2 = EMA(stockClose,period2,length)
        MACD = []
        
        for i in range(min(len(EMA1),len(EMA2))):
            MACD.append((EMA1[i]-EMA2[i])/abs(EMA2[i]))

    return MACD[entry:] 
            
def SignalLine(MACD,length):

    period = 9
    #length = 200

    signalLine = EMA(MACD,period,length)

    return signalLine

def EMA(stockClose,period,length):

    multiplier = 2./(period+1.)
    stockClose = stockClose[::-1]
    start = np.mean(stockClose[:period])
    EMA = [start]
    for i in range(min(len(stockClose)-period,length)):
        j = i+period
        
        value = (stockClose[j]-EMA[i])*multiplier + EMA[i]
        EMA.append(value)

    return EMA[::-1]

            
        
        
       
        
        

        
    
