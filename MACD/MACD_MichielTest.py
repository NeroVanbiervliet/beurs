import matplotlib.pyplot as plt
from MACDDefinitions import *
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *

stockList = loadstocklist(2)
length = 3000
resultPos = []
lenPos = []
resultNeg = []
lenNeg = []

for stock in stockList:

    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)

    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)

        MACDList = MACDCalculator(stock,0,length)
        SignalList = SignalLine(MACDList,length)[::-1]
        MACDList = MACDList[:len(SignalList)][::-1]
        
        
        
        running = True
        i = -1
        while running and i < len(SignalList)-2:
            i += 1

            if (MACDList[i] - SignalList[i]) < 0 and (MACDList[i+1] - SignalList[i+1]) > 0.01:
                
                j = i
                notFound = True
                while j < len(SignalList)-1 and notFound:
                    j += 1
                    if j == len(SignalList)-1:
                        running = False
                    else:
                        if (MACDList[j] - SignalList[j]) < 0:
                            resultPos.append((stockClose[i+1] - stockClose[j])/stockClose[j])
                            lenPos.append(j-i)
                            notFound = False

            if (MACDList[i] - SignalList[i]) > 0 and (MACDList[i+1] - SignalList[i+1]) < -0.01:
                
                j = i
                notFound = True
                while j < len(SignalList)-1 and notFound:
                    j += 1
                    if j == len(SignalList)-1:
                        running = False
                    else:
                        if (MACDList[j] - SignalList[j]) < 0:
                            resultNeg.append((stockClose[i+1] - stockClose[j])/stockClose[j])
                            lenNeg.append(j-i)
                            notFound = False
                    

            
        
