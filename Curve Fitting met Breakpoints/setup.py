from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'CurveFitDefinitions',
  ext_modules = cythonize("CurveFitDefinitions.pyx"),
)
