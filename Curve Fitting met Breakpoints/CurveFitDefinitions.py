# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import datetime
import cPickle as pickle
import time
from random import randint
import sys
sys.path.insert(0, '../Breakpoint')
from breakPointCalculator import calcBreakPoints
sys.path.insert(0, '../')
from CommonDefinitions import *


def historic(stockFile,theory, period,breakPoints,entry):
##    cdef list resultslist,theoryNew,result
##    cdef int maxDays,minDays,length,days1,days2,days3,sumdays
##    cdef float errormax
    resultslist = []
    
    if period=='Short':
        maxDays = 30
        minDays = 5
        length = 120
    if period=='Mid':
        maxDays = 30
        minDays = 15
        length = 200
    if period=='Long':
        maxDays = 50
        minDays = 30
        length = 400

    #stockFileN2 = stockfile_normaliser(stockFile,min(1500,len(stockFile)-1))[0]
    #breakPoints = calcBreakPoints(stockFileN2,length)

    sumdays = theory[3]+theory[7]+theory[11]
        
    for i in range(len(breakPoints)-3):

        days3 = breakPoints[i+1] - breakPoints[i] + 1
        days2 = breakPoints[i+2] - breakPoints[i+1]
        days1 = breakPoints[i+3] - breakPoints[i+2]

        rest1 = abs(theory[3]-days1)-int(theory[3]*0.5)
        rest2 = abs(theory[7]-days2)-int(theory[7]*0.5)
        rest3 = abs(theory[11]-days3)-int(theory[11]*0.5)
        
        if max(rest1,max(rest2,rest3)) <= 0 and breakPoints[i] >= max(entry,theory[12]) and breakPoints[i+3] < len(stockFile)-2-length:
            theoryNew = []
            theoryNew.append(theory[0])
            theoryNew.append(theory[1]*theory[3]/days1)
            theoryNew.append(theory[2]*(theory[3]/days1)**2)
            theoryNew.append(days1)
            theoryNew.append(theory[4])
            theoryNew.append(theory[5]*theory[3]/days2)
            theoryNew.append(theory[6]*(theory[3]/days2)**2)
            theoryNew.append(days2)
            theoryNew.append(theory[8])
            theoryNew.append(theory[9]*theory[3]/days3)
            theoryNew.append(theory[10]*(theory[3]/days3)**2)
            theoryNew.append(days3)
            theoryNew.append(theory[12])

            sumdaysNew = days1+days2+days3
            errormax = 0.015*(1./60.*sumdays+0.7)
            errormaxNew = 0.015*(1./60.*sumdaysNew+0.7)

            stockFileN = stockfile_normaliser2(stockFile[breakPoints[i]:],length,1)[0]
            
            # theory aangepast aan de lengte van de dagen
            if errorCalculator(stockFileN,theoryNew,sumdaysNew,errormaxNew):
                #plotTheoryVSStock(stockFileN,theoryNew,errormax)
                stockFileN2 = stockfile_normaliser2(stockFile[breakPoints[i]-theory[12]:],length,1)[0]
                result = resultAnalyser(stockFileN2,theory[12])
                resultslist.append(result)
                
            #originele theory op de nieuwe dagen   
            if errorCalculator(stockFileN,theory,sumdays,errormax):
                #plotTheoryVSStock(stockFileN,theory,errormax)
                stockFileN2 = stockfile_normaliser2(stockFile[breakPoints[i]-theory[12]:],length,1)[0]
                result = resultAnalyser(stockFileN2,theory[12])
                resultslist.append(result)
    
    return resultslist

def errorCalculator(stockFileN,theory,sumdays,errormax):
##    cdef int dayCount,running,i,j
##    cdef float theoryPiece1,theoryPiece2,theoryPiece3,error
    
    dayCount = 0
    running = 1    
    i=0    
    while i < int(len(theory)/4) and running==1:
        theoryPiece1 = theory[4*i]       
        theoryPiece2 = theory[4*i+1]       
        theoryPiece3 = theory[4*i+2]    
        error = 0.             
        j=0
        while j < theory[4*i+3] and running==1:       
            error = error + abs(stockFileN[sumdays-1 -  j - dayCount] - (theoryPiece1 + theoryPiece2*(j+min(i,1)) + theoryPiece3*(j+min(i,1))*(j+1)))
            if error/theory[4*i+3] > errormax:
                running = 0			
            j += 1
        dayCount += theory[4*i+3]
        i += 1
        
    return running

def curveFittingMethod(stock,entry,period):
    # Input: Stocknaam vb: 'Ageas'
    #        entry, hoeveel in het verleden je wilt analyseren
    #        period, welk period analyse je wilt doen vb: short,mid,long
    # Output: Expectance en Probability voor een reeks dagen
	
    expectation, probability, amount = [],[],0
    t = time.time()
    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if not check:
        print stock + 'not available'
    else:
        stockFile1 = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        if period == 'Short':
            length = 120
        if period == 'Mid':
            length = 200
        if period == 'Long':
            length = 400
           
        stockFile1N = stockfile_normaliser2(stockFile1[entry:],length,1)[0]
                 
        breakPoints = calcBreakPoints(stockFile1N,length)
        
        theory = theoryFit(stockFile1N,period,breakPoints)
        if theory:
            #plotTheoryVSStock(stockFile1N,theory,0)
            market = marketCheck(stock)

            stockList = loadstocklist(market)
            
            results = []
            sumdays = 0

            for stock2 in stockList:
                #print stock2
                stockPath = '../data/stockfiles/' + stock2 + '.txt'
                check=os.path.isfile(stockPath)
                if check:
                    stockFile2 = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
                    
                    path = '../data/breakpoint/' + stock2 + '/' + period + '.txt'
                    f = open(path,'r')
                    breakPoints = pickle.load(f)
                    f.close()
                    
                    resultsTemp = historic(stockFile2,theory,period,breakPoints,entry)
                    results.append(resultsTemp)

            expectation, probability, amount = predictor(results,theory[12])
        print 'time:',(time.time()-t)
    return expectation, probability, amount
            
        
def theoryFit(stockFileN,period,breakPoints):

    theory = []

    days3 = breakPoints[0] + 1
    days2 = breakPoints[1] - breakPoints[0]
    days1 = breakPoints[2] - breakPoints[1]

    if period == 'Short':
        minDays = 5
        maxDays = 30
        daysAfter = 30

    if max(days3,max(days2,days1)) <= maxDays and min(days3,min(days2,days1)) >= minDays:

        x3 = range(days3)
        x2 = range(days2+1)
        x1 = range(days1+1)

        [c3,b3,a3],[R3],do,re,mi = np.polyfit(x3,stockFileN[:days3][::-1],2,full=True)
        [c2,b2,a2],[R2],do,re,mi = np.polyfit(x2,stockFileN[days3-1:days2+days3][::-1],2,full=True)
        [c1,b1,a1],[R1],do,re,mi = np.polyfit(x1,stockFileN[days2+days3-1:days1+days3+days2][::-1],2,full=True)

        theory=[a1,b1,c1,days1,a2,b2,c2,days2,a3,b3,c3,days3,daysAfter]
    
    return theory
    
def predictor(results,days_after):
        
    days=[]
    amount = 0
    for i in range(days_after):
        days.append([])

    for i in range(len(results)):
        for j in range(len(results[i])):
            amount += 1
            for day in range(days_after):
                days[day].append(results[i][j][day])
                
    expectations = []
    probability = []
    if len(days[0]) > 1:
        for day in range(days_after):
            expectations.append(np.mean(days[day]))
            probability.append(1-st.norm.cdf(-np.mean(days[day])/np.std(days[day])))
        
    return expectations,probability,amount        
                
def resultAnalyser(stockfile,days_after):
    
    results_array = []
    
    for i in range(int(days_after)):
        results_array.append(max(min((stockfile[days_after-1-i] - stockfile[days_after])/(stockfile[days_after]),0.5),-0.5))
        
    return results_array        
    

def plotTheoryVSStock(stockfilepart,theory,errormax):
    theoryEvaluate = []
    
    # eerste deel van functie plotten
    for i in range(theory[3]):
        functionValue = theory[0] + theory[1]*i + theory[2]*i*i
        theoryEvaluate.append(functionValue)
    
    # tweede deel van functie plotten
    for i in range(1,theory[7]+1):
        functionValue = theory[4] + theory[5]*i + theory[6]*i*i
        theoryEvaluate.append(functionValue)
    
    # derde deel van functie plotten
    for i in range(1,theory[11]+1):
        functionValue = theory[8] + theory[9]*i + theory[10]*i*i
        theoryEvaluate.append(functionValue)
    plt.close()
    plt.plot(theoryEvaluate)
    plt.plot(stockfilepart[:(theory[3]+theory[7]+theory[11])][::-1])
    plt.title(str(errormax))
    plt.axis([0,len(theoryEvaluate)+1,0.6,1.4])
    plt.show()
    #savePath = 'data/results/tests/' + types + str(graphname)
    #plt.savefig(savePath)



