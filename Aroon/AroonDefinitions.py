# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
from random import randint
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *

def AroonScore(stock,entry):
    scoreList = []
    period = 25
    AroonUp ,AroonDown = AroonCalculator(stock,entry,period)
    upScore = 0
    downScore = 0

    AroonUp = AroonUp[::-1]
    AroonDown = AroonDown[::-1]
    
    for i in range(1,len(AroonUp)):
        
        upScore = upScore*0.6
        downScore = downScore*0.6
        basisScore = (AroonUp[i] - AroonDown[i])*0.1

        if AroonUp[i] == 100 and AroonUp[i-1] < 40 and AroonDown[i] < 50:
            upScore = 20
            
        if AroonDown[i] == 100 and AroonDown[i-1] < 40 and AroonUp[i] < 50:
            downScore = 20

        score = basisScore + upScore + downScore
        scoreList.append(score)
        
    return scoreList[::-1]


def AroonCalculator(stock,entry,period):

    AroonUp = []
    AroonDown = []

    maxEntry = 9999
    minEntry = 9999

    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)[entry:]
        for i in range(len(stockClose)-period):
            j = len(stockClose)-period-1-i
            
            if stockClose[j] > max(stockClose[j+1:j+1+period]):
                maxValue = stockClose[j]
                maxEntry = j
            if stockClose[j] < min(stockClose[j+1:j+1+period]):
                minValue = stockClose[j]
                minEntry = j
                
            AroonUpValue = (period-(maxEntry-j))/float(period)*100.
            AroonDownValue = (period-(minEntry-j))/float(period)*100.
            
            AroonUp.append(max(AroonUpValue,0))
            AroonDown.append(max(AroonDownValue,0))
            

    return AroonUp[::-1],AroonDown[::-1]

        
    
