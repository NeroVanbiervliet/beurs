import numpy as np
from numpy import loadtxt
import scipy.stats as st
import os.path
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
sys.path.insert(0, '../MACD')
from MACDDefinitions import *
sys.path.insert(0, '../BollingerBand')
from BBDefinitions import *
sys.path.insert(0, '../RSI')
from RSIDefinitions import *
sys.path.insert(0, '../Download')
from Download import Download
from SimulatorDefinitions import Sim,find,loadScores


#parameters voor beglische aandelen
BBScale = 5.3
MACDScale = 0.5
RSIScale = 0.1
totalThreshold = 30.

stockList = loadstocklist(1) #belgische aandelen laden
gainList = []
stockData = []

RSIScoreList,BBScoreList,MACDScoreList,gainList2 = loadScores(stockList,1,1500,6)

for stock in stockList:
    stockPath = '../data/stockfiles/' + stock + '.txt'
    stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
    stockData.append(stockClose[:1500]) 

count = 0
while count < 200:
    count += 1
    entry = count
    print 'count:', count
    
    for j in range(len(stockList)):
        stock = stockList[j]        
        #stockPath = '../data/stockfiles/' + stock + '.txt'
        #check=os.path.isfile(stockPath)
        check = True
        if check:
            stockClose = stockData[j]
            BBScoreValue = BBScoreList[j][entry]
            MACDScoreValue = MACDScoreList[j][entry]
            RSIScoreValue = RSIScoreList[j][entry]

            totalScore = BBScoreValue*BBScale + MACDScoreValue*MACDScale + RSIScoreValue*RSIScale

            if totalScore > totalThreshold:
                gain = (stockClose[entry-6+6] - stockClose[entry+6])/stockClose[entry+6]
                #gain = gainList2[j][entry]
                gainList.append(gain)
                

        




