from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
import random
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
sys.path.insert(0, '../MACD')
from MACDDefinitions import *
sys.path.insert(0, '../BollingerBand')
from BBDefinitions import *
sys.path.insert(0, '../RSI')
from RSIDefinitions import *
sys.path.insert(0, '../Aroon')
from AroonDefinitions import *
from SimulatorDefinitions import Sim,find,loadScores

stockList = loadstocklist(2) #lijst met aandelen laden
count = 0

periodAfter = 6

daysInThePast = 1000
ratio = 0.8

## Welke indicators doen mee
indicators = [1,1,1,0,1]
#indicators = [BB,RSI,MACD,trend,Aroon]

## Alle data voor de gekozen aandelen opslaan

Scores, gainList = loadScores(stockList,ratio,daysInThePast,periodAfter,indicators)

print "Data ready"
print "Start optimization"

## Alle nodige parameters berekenen (optimalisatie)

# Minimum vereisten voor de optimalisatie
kansOpWinst = 1.
averageGain = 0.0
minAmount = 0.

# Parameters die worden getuned
BBScale = -2
RSIScale = 2.
MACDScale = 2.
TrendScale = 1.
AroonScale = 1.

parameters = []
parametersOriginal = [BBScale,RSIScale,MACDScale,TrendScale,AroonScale]
for i in range(len(indicators)):
    if indicators[i]:
        parameters.append(parametersOriginal[i])


# Andere Constantes
totalThreshold = len(Scores)*10.

# variablen declareren
running = True
delta = np.concatenate([np.ones(len(Scores)), np.ones(len(Scores))*(-1)])
precisionFactorStart = 0.1
precisionFactor = precisionFactorStart
bestValue = -99999.
count = 0
opt = 0

amountSamples = daysInThePast*ratio*len(stockList)
percetage = 0.005
print amountSamples*percetage

while running:
    count +=1    
       
    P,P2,E,n,data = Sim(Scores,gainList,parameters,totalThreshold,stockList,0)
    
    
    bestValue = P/2. + E + min((n-amountSamples*percetage),0)

    if opt ==0:
        print ' '
        print count
        print parameters
        print P,P2,E,n
        print bestValue

    diffList = []
    for i in range(len(delta)):
        parametersTemp = list(parameters)
        parametersTemp[i%len(Scores)] = parametersTemp[i%len(Scores)] + delta[i]*precisionFactor
        
        PTemp,P2Temp,ETemp,nTemp,data = Sim(Scores,gainList,parametersTemp,totalThreshold,stockList,0)

        value = PTemp/2. + ETemp + min((nTemp-amountSamples*percetage),0)
        valueDiff = value - bestValue
        diffList.append(valueDiff)

    if max(diffList)> 0.00001:
        opt = 0
        entry = find(max(diffList),diffList)
        parameters[entry%len(Scores)] = parameters[entry%len(Scores)] + delta[entry]*precisionFactor
        precisionFactor = precisionFactorStart
    

    else:
        
        if opt == 8:
            print 'optimum found'
            print parameters
            print P,P2,E,n
            running = False
        else:
            precisionFactor += 0.5
            opt += 1
    

## Validatie van de gevonden Parameters, P en E op andere data    


daysInThePast2 = int((1-ratio)*daysInThePast)
ratio2 = 1

Scores2, gainList2 = loadScores(stockList,ratio2,daysInThePast2,periodAfter,indicators)

PVal,P2Val,EVal,nVal,dataVal = Sim(Scores2,gainList2,parameters,totalThreshold,stockList,0)

print PVal,P2Val,EVal,nVal

            

        
        
    

            

        
