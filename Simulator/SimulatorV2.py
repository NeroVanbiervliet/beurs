# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
import random
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
sys.path.insert(0, '../MACD')
from MACDDefinitions import *
sys.path.insert(0, '../BollingerBand')
from BBDefinitions import *
sys.path.insert(0, '../RSI')
from RSIDefinitions import *
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import AxesGrid
import matplotlib


from scipy.interpolate import griddata


def MACDScore(stock): #definieer nieuwe score functie voor deze simulatie speciaal

    days = 8 #som van 8 dagen
    score = []
    diffList = []
    entry = 0
    length = 9999999 #berekenen voor alle dagen
    multiplier = 500. #eindscore wordt maal dit getal gedaan

    MACD = MACDCalculator(stock,entry,length) #bereken MACD
    signalLine = SignalLine(MACD,length) #bereken signal line

    for i in range(len(signalLine)):
        
        diffList.append(MACD[i]-signalLine[i]) #bereken verschil van 1 dag

    for i in range(len(diffList)-days):
        cross = False
        scoreValue = 0
        for j in range(days):
            if diffList[i+days-j] < 0 and diffList[i+days-1-j] > 0:
                cross = True
                scoreValue = 0
            if diffList[i+days-j] > 0 and diffList[i+days-1-j] < 0:
                cross = False
                scoreValue = 0
            if cross:
                scoreValue += diffList[i+days-1-j]*multiplier

        score.append(scoreValue)
    
    return score

def BBScore(stock):
    days = 8 #som van 8 dagen
    entry = 0
    length = 999999
    diffList = []
    score = []
    multiplier = 20.
    
    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath) #kijk of het aandeel bestaat
    
    if check: #als het aandeel bestaat
        BB = BollingerBandValue(stock,entry,length) #bereken bollinger waardes
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False) #aandeel data laden

        for i in range(len(BB)):
            diffList.append((BB[i] - stockClose[i])/stockClose[i]) # bereken verschil per dag

        for i in range(len(diffList)-days):
            
            cross = False
            scoreValue = 0
            for j in range(days):
                if diffList[i+days-j] < 0 and diffList[i+days-1-j] > 0:
                    cross = True
                    scoreValue = 0
                if diffList[i+days-j] > 0 and diffList[i+days-1-j] < 0:
                    cross = False
                    scoreValue = 0
                if cross:
                    scoreValue += diffList[i+days-1-j]*multiplier

            score.append(scoreValue)

    return score

stockList = loadstocklist(1) #lijs tmet aandelen laden
count = 0
 
period = 6 #dagen dat het script erna gaat kijken om de winst/verlies te bepalen
period2 = 8 #dagen waarvan de som van scores wordt genomen

results = []
scores = []
xlist = []
ylist = []
zlist = []

totalList = []
MACDlist = []
bblist = []
rsilist = []
stocksave = []
iList = []
a = 1. #MACd parameter
b = 1. #BB parameter
c = 1. #RSi parameter
count = 0

for stock in stockList: #elk aandeel afgaan
    print stock    
    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        ## alle indicators laden die we nodig hebben
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        BBScoreList = BBScore(stock)
        MACDScoreList = MACDScore(stock)
        RSIScoreList = RSIScore(stock,0)
        MACD = MACDCalculator(stock,0,999999999)
        BB = BollingerBandValue(stock,0,999999999)
        signalLine = SignalLine(MACD,99999999)

        for i in range(period,min(min(len(BBScoreList),min(len(MACDScoreList),len(RSIScoreList))),1000)): #alle dagen van het aandeel overlopen
            diff = 0.
            for j in range(1,15):
                diff += (stockClose[i+j-1] - stockClose[i+j])/stockClose[i+j] #percentage verschil berekenen
            diff = diff/15. #gemiddelde nemen
            
            if diff > -0.005: #als de trend niet teveel naar beneden is
##                ## checken dat MACD en BB de koers doorbreken
##                bingo1 = 0
##                bingo2 = 0
##                for j in range(period2-1):
##                    diff1 = MACD[i+j] - signalLine[i+j]
##                    diff2 = MACD[i+j+1] - signalLine[i+j+1]
##                    if diff1*diff2 < 0:
##                        bingo1 += 1
##                    diff3 = BB[i+j] - stockClose[i+j]
##                    diff4 = BB[i+j+1] - stockClose[i+j+1]
##                    if diff3*diff4 < 0:
##                        bingo2 += 1
##                        
##                if bingo1 == 1 and bingo2 == 1: #als ze allebei de koers/signalline doorbreken

                if MACDScoreList[i] > 0 and BBScoreList[i] > 0: #allebei groter dan 0 wilt zeggen dat ze koers doorbroken zijn

                    totalScore = a*MACDScoreList[i] + b*BBScoreList[i] + c*RSIScoreList[i] 

                    #if min(BBScoreList[i-period+1:i+1]) >= 0 and min(MACDScoreList[i-period+1:i+1]) >= 0 and min(RSIScoreList[i-period+1:i+1]) >= 0: #als ze tijdens het kopen niet onder 0 gaan, dus geen verkoop signaal
                    #alle data in lijstjes opslaan
                    totalList.append(totalScore)
                    MACDlist.append(MACDScoreList[i])
                    bblist.append(BBScoreList[i])
                    rsilist.append(RSIScoreList[i])
                    stocksave.append(stock)
                    iList.append(i)

                    gain = (stockClose[i-period] - stockClose[i])/stockClose[i]*100. #percentage winst, merk de *100 op
                    results.append(gain)
                    count +=1
                    #waardes in de matrix gieten zodanig dat die later kan gebruikt wordne voor linear regression
                    if count ==1:
                       A = [[MACDScoreList[i],BBScoreList[i],RSIScoreList[i],1]]
                       B = [gain]
                    else:
                        A = np.append(A,[[MACDScoreList[i],BBScoreList[i],RSIScoreList[i],1]],axis=0)
                        B = np.append(B,gain)

#linear regression    
At = np.transpose(A)
Anew = np.dot(At,A)
Bnew = np.dot(At,B)
x = np.linalg.solve(Anew, Bnew)
print x
              
##while count < 100:
##   
##    stock = stockList[random.randint(0,len(stockList)-1)]
##    
##    stockPath = '../data/stockfiles/' + stock + '.txt'
##    check=os.path.isfile(stockPath)
##
##    if check:
##        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
##
##        if (len(stockClose) - entry - 300) > 0:
##            MACDScoreValue,BBScoreValue,RSIScoreValue = ScoreCalculator(stock,entry)
##
##            if abs(MACDScoreValue) > 0 and abs(BBScoreValue) > 0 and abs(RSIScoreValue) > 0:
##
##                count +=1
##                
##                print count
##                    
##                gain = (stockClose[entry-period] - stockClose[entry])/stockClose[entry]*100.
##                results.append(gain)
##                scores.append([MACDScoreValue,BBScoreValue,RSIScoreValue])
##                xlist.append(MACDScoreValue)
##                ylist.append(BBScoreValue)
##                zlist.append(RSIScoreValue)
##                if count ==1:
##                    A = [[MACDScoreValue,BBScoreValue,RSIScoreValue,1]]
##                    b = [gain]
##                else:
##                    A = np.append(A,[[MACDScoreValue,BBScoreValue,RSIScoreValue,1]],axis=0)
##                    b = np.append(b,gain)
##                    
##At = np.transpose(A)
##Anew = np.dot(At,A)
##bnew = np.dot(At,b)
##x = np.linalg.solve(Anew, bnew)
##print x
##
###plt.scatter(scores,results)
###plt.show()
##
##fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')
##ax.scatter(xlist, ylist, zlist,c=results)
##
##ax.set_xlim(-20,20)
##ax.set_ylim(-20,20)
##ax.set_zlim(-20,20)
##ax = plt.gca()
##ax.set_ylim(ax.get_ylim()[::-1])
##plt.show()
        

            
            

        
