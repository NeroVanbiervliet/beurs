# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
import random
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
sys.path.insert(0, '../MACD')
from MACDDefinitions import *
sys.path.insert(0, '../BollingerBand')
from BBDefinitions import *
sys.path.insert(0, '../RSI')
from RSIDefinitions import *
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import AxesGrid
import matplotlib


from scipy.interpolate import griddata

stockList = loadstocklist(0)
count = 0

period = 6

results = []
scores = []
xlist = []
ylist = []
zlist = []



def ScoreCalculator(stock,entry):

    a = 1.
    b = 1.
    c = 1.
    
    MACDScoreValue = MACDScore(stock,entry)[0]
    BBScoreValue = BollingerBandScore(stock,entry)[0]
    RSIScoreValue = RSIScore(stock,entry)[0]


##    #print stock
##    #print round(MACDScoreValue,2)
##    #print round(BBScoreValue,2)
##    #print round(RSIScoreValue,2)
##    dummy=1
##    if int(MACDScoreValue)==0 or int(BBScoreValue)==0:
##        dummy=0
##    
##    totalScore = a*MACDScoreValue + b*BBScoreValue + c*RSIScoreValue*dummy
##
##    if totalScore > 50:
##        print stock,entry
##        print totalScore 
##    
##
##    #print totalScore

    return MACDScoreValue,BBScoreValue,RSIScoreValue 

while count < 100:
   
    stock = stockList[random.randint(0,len(stockList)-1)]
    entry = random.randint(100,1000)

    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)

    if check:
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)

        if (len(stockClose) - entry - 300) > 0:
            MACDScoreValue,BBScoreValue,RSIScoreValue = ScoreCalculator(stock,entry)

            if abs(MACDScoreValue) > 0 and abs(BBScoreValue) > 0 and abs(RSIScoreValue) > 0:

                count +=1
                
                print count
                    
                gain = (stockClose[entry-period] - stockClose[entry])/stockClose[entry]*100.
                results.append(gain)
                scores.append([MACDScoreValue,BBScoreValue,RSIScoreValue])
                xlist.append(MACDScoreValue)
                ylist.append(BBScoreValue)
                zlist.append(RSIScoreValue)
                if count ==1:
                    A = [[MACDScoreValue,BBScoreValue,RSIScoreValue,1]]
                    b = [gain]
                else:
                    A = np.append(A,[[MACDScoreValue,BBScoreValue,RSIScoreValue,1]],axis=0)
                    b = np.append(b,gain)
                    
At = np.transpose(A)
Anew = np.dot(At,A)
bnew = np.dot(At,b)
x = np.linalg.solve(Anew, bnew)
print x

#plt.scatter(scores,results)
#plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(xlist, ylist, zlist,c=results)

ax.set_xlim(-20,20)
ax.set_ylim(-20,20)
ax.set_zlim(-20,20)
ax = plt.gca()
ax.set_ylim(ax.get_ylim()[::-1])
plt.show()
        

            
            

        
