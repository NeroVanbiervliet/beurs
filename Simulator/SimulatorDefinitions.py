import numpy as np
from numpy import loadtxt
import scipy.stats as st
import os.path
import time
import sys
import matplotlib.pyplot as plt
sys.path.insert(0, '../')
from CommonDefinitions import *
sys.path.insert(0, '../MACD')
from MACDDefinitions import *
sys.path.insert(0, '../BollingerBand')
from BBDefinitions import *
sys.path.insert(0, '../RSI')
from RSIDefinitions import *
sys.path.insert(0, '../Trend')
from TrendDefinitions import *
sys.path.insert(0, '../Aroon')
from AroonDefinitions import *

def loadScores(stockList,ratio,daysInThePast,periodAfter,indicators):

    gainList = []
    RSIScoreList = []
    BBScoreList = []
    MACDScoreList = []
    TrendScoreList = []
    AroonScoreList = []
    
    dummy = int((1-ratio)*daysInThePast)

    for stock in stockList:
    
        #print stock
        stockPath = '../data/stockfiles/' + stock + '.txt'
        check=os.path.isfile(stockPath)
        if check:
            ## alle indicators laden die we nodig hebben
            stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
            if indicators[0]:
                BBScoreList.append(BollingerBandScore(stock,periodAfter,daysInThePast)[dummy:])
            if indicators[1]:
                RSIScoreList.append(RSIScore(stock,periodAfter)[dummy:])
            if indicators[2]:  
                MACDScoreList.append(MACDScore(stock,periodAfter,daysInThePast)[dummy:])
            if indicators[3]:
                TrendScoreList.append(TrendScore(stock,periodAfter,daysInThePast)[dummy:])
            if indicators[4]:
                AroonScoreList.append(AroonScore(stock,periodAfter)[dummy:])
    
            #RandomList.append(RandomScore(daysInThePast)[dummy:])

            gainTemp = []
            for i in range(min(len(stockClose)-periodAfter,daysInThePast)):
                gainTemp.append(min(max((stockClose[i] - stockClose[i+periodAfter])/stockClose[i+periodAfter],-0.2),0.2))
            gainList.append(gainTemp[dummy:])
            
    Scores = []
    
    if indicators[0]:
        Scores.append(BBScoreList)
    if indicators[1]:
        Scores.append(RSIScoreList)
    if indicators[2]:
        Scores.append(MACDScoreList)
    if indicators[3]:
        Scores.append(TrendScoreList)
    if indicators[4]:
        Scores.append(AroonScoreList)
    
        
    return Scores,gainList


def Sim(Scores,gainList,parameters,totalThreshold,stockList,plotStatus):
    name = 0
    result = []
    data = []
    t0 = time.time()
    for i in range(len(gainList)):
        
        stock = stockList[i]
        prevj = -10
        
        dummy = 999999.
        for j in range(len(Scores)):
            dummy = min(dummy,len(Scores[j][i]))
        
        for j in range(dummy):
            ScoreValues = []
            for k in range(len(Scores)):
                ScoreValues.append(Scores[k][i][:dummy][::-1][j])
            gain= gainList[i][:dummy][::-1][j]
            
            totalScore = 0.
            for k in range(len(ScoreValues)):
                totalScore += ScoreValues[k]*parameters[k]

            if totalScore > totalThreshold:
                if prevj + 1 < j:
                    result.append(gain)
                    data.append([stock,dummy-1-j])
                    if plotStatus and j > 20:
                        
                        plt.subplot(321)
                        plt.plot(Scores[0][i][:dummy][::-1][j-20:j])
                        plt.ylabel('BB')
                        plt.subplot(322)
                        plt.plot(Scores[1][i][:dummy][::-1][j-20:j])
                        plt.ylabel('RSI')
                        plt.subplot(323)
                        plt.plot(Scores[2][i][:dummy][::-1][j-20:j])
                        plt.ylabel('MACD')
                        plt.subplot(324)
                        plt.plot(Scores[3][i][:dummy][::-1][j-20:j])
                        plt.ylabel('Trend')
                        plt.subplot(325)
                        plt.plot(Scores[4][i][:dummy][::-1][j-20:j])
                        plt.ylabel('Aroon')
                        plt.subplot(326)
                        plt.plot(gainList[i][:dummy][::-1][j-20:j])
                        plt.ylabel('gain')
                        
                        plt.savefig('results/' + str(name) + '_' + str(totalScore) + '.png')
                        name = name + 1
                        plt.close()
                        
                prevj = j
                
    #print 'time:', time.time()-t0
    
    if len(result)>1:
        E = np.mean(result)
        P = 1-st.norm.cdf(-np.mean(result)/np.std(result))
        
        positiveCount = 0
        for i in range(len(result)):
            if result[i] > 0:
                positiveCount += 1
        P2 = float(positiveCount)/float(len(result))   
        
        n = len(result)
    else:
        E = 0
        P = 0
        P2 = 0
        n = 0


    return P,P2,E,n,data

def find(value,list):
    
    for i in range(len(list)):
        if abs(value - list[i]) < 0.0000000001:
            
            entry = i
            
    return entry
