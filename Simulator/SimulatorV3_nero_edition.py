# -*- coding: cp1252 -*-
from numpy import loadtxt
import numpy as np
import os.path
import matplotlib.pyplot as plt
import scipy.stats as st
import time
import random
import sys
sys.path.insert(0, '../')
from CommonDefinitions import *
sys.path.insert(0, '../MACD')
from MACDDefinitions import *
sys.path.insert(0, '../BollingerBand')
from BBDefinitions import *
sys.path.insert(0, '../RSI')
from RSIDefinitions import *
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import AxesGrid
import matplotlib
from scipy.interpolate import griddata


stockList = loadstocklist(1) #lijst met aandelen laden
count = 0
 
period = 6 #dagen dat het script erna gaat kijken om de winst/verlies te bepalen
period2 = 8 #dagen waarvan de som van scores wordt genomen


totalList = []

a = 0.0496 #MACd parameter
b = -0.345 #BB parameter
c = 0.005 #RSi parameter
count = 0
       
            
for stock in stockList: #elk aandeel afgaan
    print stock    
    stockPath = '../data/stockfiles/' + stock + '.txt'
    check=os.path.isfile(stockPath)
    if check:
        ## alle indicators laden die we nodig hebben
        stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)
        BBScoreList = BollingerBandScore(stock,0)
        MACDScoreList = MACDScore(stock,0)
        RSIScoreList = RSIScore(stock,0)
        

        for i in range(period,min(min(len(BBScoreList),min(len(MACDScoreList),len(RSIScoreList))),1000)): #alle dagen van het aandeel overlopen
            
            # trendberekening            
            diff = 0.
            for j in range(1,15):
                diff += (stockClose[i+j-1] - stockClose[i+j])/stockClose[i+j] #percentage verschil berekenen
            diff = diff/15. #gemiddelde nemen
            
            if diff > -0.005: #als de trend niet teveel naar beneden is
            
                    totalScore = (a*MACDScoreList[i] + b*BBScoreList[i] + c*RSIScoreList[i])
                    
                              
                    
                    if totalScore <1000 and totalScore>-20:                    
                    
                        count +=1                     
                    
                        totalList.append(totalScore)          
                        
                        gain = (stockClose[i-period] - stockClose[i])/stockClose[i]*100. #percentage winst, merk de *100 op
                        
                        # TODO: baert, waarom definieer je niet gewoon A en B buiten de lus en append je dan altijd? 
                    
                        #waardes in de matrix gieten zodanig dat die later kan gebruikt worden voor linear regression
                        if count ==1: # initialisatie bij eerste keer
                           A = [[MACDScoreList[i],BBScoreList[i],RSIScoreList[i],1]] #1 is de constante term
                           B = [gain]
                        else: # append bij volgende keren
                            A = np.append(A,[[MACDScoreList[i],BBScoreList[i],RSIScoreList[i],1]],axis=0)
                            B = np.append(B,gain)

#linear regression    
At = np.transpose(A)
Anew = np.dot(At,A)
Bnew = np.dot(At,B)
x = np.linalg.solve(Anew, Bnew)
print x

# scatter of gain en total score
plt.scatter(totalList,B)
plt.xlabel('total score')
plt.ylabel('gain')
plt.axhline(y=0)