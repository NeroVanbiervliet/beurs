# deze file bevat groepen van stocks, steeds met beschrijving en een naam

# deze functie geeft een lijst terug van stocks
def getStockGroup(stockGroupName):
   
   return {
        
        # VOORBEELD europeFarma (nero)
        # europese bedrijven ing de farmaceutische sector
        # bedrijven: Roche (ROG), GlaxoSmithKline (GSK)
        'europeFarma': ['ROG','GSK'],
      
        # VOORBEELD belStaal (nero)
        # belgische staalbedrijven
        # bedrijven: ArcelorMittal (MT)
        'belStaal': ['MT']
    }[stockGroupName]
    
    
