import numpy as np
import sys
sys.path.insert(0, '../')

# eigen gemaakt methode voor de trendindicatie
# stockClose is closing value van een stock
def TrendCalculator(stockClose,entry):
    
    diff = 0.
    for j in range(1,10):
        diff += (stockClose[entry+j-1] - stockClose[entry+j])/stockClose[entry+j] #percentage verschil berekenen
    diff = diff/10. #gemiddelde nemen
    
    return diff
 
# eigen gemaakt methode voor de trendindicatie
# stock wordt verondersteld te verwijzen naar een bestaande stockfile  
def TrendScore(stock,entry,length):
    stockPath = '../data/stockfiles/' + stock + '.txt'
    stockClose = np.loadtxt(stockPath, delimiter=',', skiprows=1, usecols=(4,), unpack=False)

    result = []
    threshold = 0.001
    
    for i in range(1,min(length,len(stockClose)-16)):
        
        value =  TrendCalculator(stockClose,i)*500.
                
        result.append(value)
        
    return result[entry:]
